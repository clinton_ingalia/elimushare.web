/**
 * Created by clintonb on 4/23/2017.
 */
'use strict';

angular.module('ElimuShare.events', ['ngRoute', 'ngStorage'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/events', {
            templateUrl: 'events/events.html',
            controller: 'EventsCtrl'
        });
    }])

    .controller('EventsCtrl', ['$scope', 'toaster', '$http', '$location', '$sessionStorage', 'myUrl', function ($scope, toaster, $http, $location, $sessionStorage, myUrl) {
        if ($sessionStorage.UserId != null && $sessionStorage.SchoolId != null) {
            $http.get(myUrl + "api/SchoolByAdministratorId?AdministratorId=" + $sessionStorage.UserId).then(function (response) {
                var results = response.data;
                console.log(response.data);

                $scope.LogoUrl = results.LogoUrl;
                $scope.Name = results.Name;
                $sessionStorage.SchoolId = results.ID;
                $sessionStorage.LogoUrl = $scope.LogoUrl;
                $sessionStorage.Name = $scope.Name;

                $scope.loadPageFunctions();
            }, function (response) {
                //toaster.pop('error', response.data, "Check mobile number or password");
            });
        } else {
            $location.path('/login');
        }

        $scope.loadPageFunctions = function () {
            $http.get(myUrl + "api/DTventsBySchoolId?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {
                var results = response.data;
                $scope.dataTemplate = {
                    text: "",
                    ownerId: [],
                    startDate: "",
                    endDate: ""
                }
                var data = [];

                angular.forEach(results, function (value, key) {
                    $scope.dataTemplate = {
                        text: "",
                        ownerId: [],
                        startDate: "",
                        endDate: ""
                    }
                    $scope.dataTemplate.text = value.text;
                    $scope.dataTemplate.ownerId = value.ownerId;
                    $scope.dataTemplate.startDate = new Date(value.startDate);
                    $scope.dataTemplate.endDate = new Date(value.endDate);
                    data.push($scope.dataTemplate)
                });
                $scope.schedulerOptions = {
                    dataSource: data,
                    views: ["agenda", "month", "week", "workWeek", "day"],
                    currentView: "month",
                    currentDate: new Date(),
                    useDropDownViewSwitcher: false,
                    firstDayOfWeek: 0,
                    startDayHour: 8,
                    endDayHour: 19,
                    bindingOptions: {
                        editing: "editing",
                        useDropDownViewSwitcher: "useDropDownViewSwitcher"
                    },
                    resources: [{
                        fieldExpr: "ownerId",
                        label: "Owner",
                        allowMultiple: true,
                        dataSource: resourcesData
                    }],
                    onAppointmentAdding: function (e) {
                        $scope.appData = {
                            SchoolId: $sessionStorage.SchoolId,
                            Title: e.appointmentData.text,
                            ownerId: e.appointmentData.ownerId,
                            Description: e.appointmentData.description,
                            LocationName: "Not set",
                            Latitude: 5.1,
                            Longitude: 6.1,
                            Time: "2017-05-17T08:29:14.2159036+00:00",
                            StartDate: e.appointmentData.startDate,
                            EndDate: e.appointmentData.endDate
                        }
                        console.log(e);
                        $http.post(myUrl + "api/Events", $scope.appData).then(function (response) {
                            toaster.pop('success', "Success", "Event added");
                        }, function (response) {
                            //toaster.pop('error', response.data, "Check mobile number or password");
                        });
                    },
                    width: "100%",
                    height: 600
                };
            }, function (response) {
                //toaster.pop('error', response.data, "Check mobile number or password");
            });
        }




        var resourcesData = [
            {
                text: "All Staff",
                id: "1",
                color: "#cb6bb2"
            }, {
                text: "Administrative Staff",
                id: 2,
                color: "#56ca85"
            }, {
                text: "Teaching Staff",
                id: 3,
                color: "#1e90ff"
            }, {
                text: "Non Teaching Staff",
                id: 4,
                color: "#ff9747"
            }
        ];


        $scope.editing = {
            allowAdding: true,
            allowUpdating: true,
            allowDeleting: true,
            allowResizing: true,
            allowDragging: true
        };
        $scope.useDropDownViewSwitcher = false;
        $scope.disabledValue = false;

        var switchModeNames = ["Tabs", "Drop-Down Menu"];

        $scope.useDropDownViewSwitcherOptions = {
            items: switchModeNames,
            width: 200,
            value: switchModeNames[0],
            onValueChanged: function (data) {
                $scope.useDropDownViewSwitcher = data.value === switchModeNames[1];
            }
        };
        $scope.allowAddingOptions = {
            text: "Allow adding",
            bindingOptions: {
                value: "editing.allowAdding"
            }
        };
        $scope.allowUpdatingOptions = {
            text: "Allow updating",
            onValueChanged: function (data) {
                $scope.disabledValue = !data.value;
            },
            bindingOptions: {
                value: "editing.allowUpdating"
            }
        };
        $scope.allowDeletingOptions = {
            text: "Allow deleting",
            bindingOptions: {
                value: "editing.allowDeleting"
            }
        };
        $scope.allowResizingOptions = {
            text: "Allow resizing",
            bindingOptions: {
                value: "editing.allowResizing",
                disabled: "disabledValue"
            }
        };
        $scope.allowDraggingOptions = {
            text: "Allow dragging",
            bindingOptions: {
                value: "editing.allowDragging",
                disabled: "disabledValue"
            }
        };


    }]);