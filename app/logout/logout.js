/**
 * Created by clintonb on 4/13/2017.
 */
'use strict';

angular.module('ElimuShare.logout', ['ngRoute', 'toaster', 'ngAnimate', 'ngStorage'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/logout', {
            templateUrl: 'logout/logout.html',
            controller: 'LogoutCtrl'
        });
    }])

    .controller('LogoutCtrl', ['$scope', 'toaster', '$http', '$location', '$sessionStorage', function ($scope, toaster, $http, $location, $sessionStorage) {
        $sessionStorage.$reset();
        $location.path('/login');
    }]);