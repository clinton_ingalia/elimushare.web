/**
 * Created by clintonb on 4/23/2017.
 */
'use strict';

angular.module('ElimuShare.fees', ['ngRoute', 'ngStorage'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/fees', {
            templateUrl: 'fees/fees.html',
            controller: 'FeesCtrl'
        });
    }])

    .controller('FeesCtrl', ['$scope', 'toaster', '$http', '$location', '$sessionStorage', 'myUrl', function ($scope, toaster, $http, $location, $sessionStorage, myUrl) {
        if ($sessionStorage.UserId != null && $sessionStorage.SchoolId != null) {
            $http.get(myUrl + "api/SchoolByAdministratorId?AdministratorId=" + $sessionStorage.UserId).then(function (response) {
                var results = response.data;

                $scope.LogoUrl = results.LogoUrl;
                $scope.Name = results.Name;
                $sessionStorage.SchoolId = results.ID;
                $sessionStorage.LogoUrl = $scope.LogoUrl;
                $sessionStorage.Name = $scope.Name;

                $scope.loadPageFunctions();

            }, function (response) {
                toaster.pop('error', 'Error', "Could not load data, check your internet connectivity");
            });
        } else {
            $location.path('/login');
        }


        $scope.loadPageFunctions = function () {
            $http.get(myUrl + "api/DxFeesBySchoolId?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {
                var results = response.data;

                $scope.fees = results;

            }, function (response) {
                toaster.pop('error', 'Info', "Could not load fees, please try again, if the issue persists contact system admin");
            });


            $http.get(myUrl + "api/StudentsBySchoolId?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {
                var results = response.data;
                $scope.studentData = results;
                $scope.addStudent = {
                    studentLookup: {
                        dataSource: $scope.studentData,
                        valueExpr:"ID",
                        displayExpr:"SurName",
                        searchEnabled: true,
                        placeholder: "  [Select One]",
                        visible: true,
                        title: "Students",
                        itemTemplate: function (data, index, container) {
                            var row = $("<div>").addClass("row-fluid");
                            $("<div>").addClass("col-xs-3").text(data["FirstName"]).appendTo(row);
                            $("<div>").addClass("col-xs-3").text(data["SecondName"]).appendTo(row);
                            $("<div>").addClass("col-xs-3").text(data["SurName"]).appendTo(row);
                            container.append(row);

                        },
                        showPopupTitle: true
                    }
                }

            }, function (response) {
                if(response.status == 404){
                    //toaster.pop('info', 'Error', "No data found");
                }else if(response.status == 500){
                    toaster.pop('error', 'Info', "Something went wrong please try again, if the issue persists contact system admin");
                } else{
                    toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                }

            });


            $http.get(myUrl + "api/PaymentMethodsBySchoolId?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {
                var results = response.data;
                $scope.paymentMethodData = results;
                $scope.addPaymentMethod = {
                    paymentMethodLookup: {
                        dataSource: $scope.paymentMethodData,
                        valueExpr:"ID",
                        displayExpr:"Name",
                        searchEnabled: true,
                        placeholder: "  [Select One]",
                        visible: true,
                        title: "Parents",
                        itemTemplate: function (data, index, container) {
                            var row = $("<div>").addClass("row-fluid");
                            $("<div>").addClass("col-xs-5").text(data["Name"]).appendTo(row);
                            $("<div>").addClass("col-xs-3").text(data["AccountName"]).appendTo(row);
                            $("<div>").addClass("col-xs-3").text(data["AccountNumber"]).appendTo(row);
                            container.append(row);

                        },
                        showPopupTitle: true
                    }
                }

            }, function (response) {
                if(response.status == 404){
                    //toaster.pop('info', 'Error', "No data found");
                }else if(response.status == 500){
                    toaster.pop('error', 'Info', "Something went wrong please try again, if the issue persists contact system admin");
                } else{
                    toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                }

            });

        }


        $scope.Term = {
            Type: {
                items: [1, 2, 3],
                searchEnabled: true,
                placeholder: "  [Select One]",
                visible: true,
                title: "Class Level",
                showPopupTitle: true
            }
        }


        $scope.delete = function (data) {
            $scope.feeDeleteId = data.ID;
        }

        $scope.saveDelete = function (data) {
            $http.delete(myUrl + "api/Fees/" + $scope.feeDeleteId).then(function (response) {
                $scope.fees = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-deleteFee').modal('hide');
                toaster.pop('success', 'Record Deleted', "Record successfully deleted");
            }, function (response) {

                toaster.pop('error', 'Error', "Could not delete record. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
            });
        }

        $scope.deletePM = function (data) {
            $scope.pmDeleteId = data.ID;
        }

        $scope.saveDeletePM = function () {
            $http.delete(myUrl + "api/PaymentMethods/" + $scope.pmDeleteId).then(function (response) {
                $scope.fees = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-deletePM').modal('hide');
                toaster.pop('success', 'Record Deleted', "Record successfully deleted");
            }, function (response) {

                toaster.pop('error', 'Error', "Could not delete record. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
            });
        }

        $scope.studentValidationRules = {
            validationRules: [{
                type: "required",
                message: "Student is required"
            }]
        };

        $scope.paymentMethodValidationRules = {
            validationRules: [{
                type: "required",
                message: "Payment Method is required"
            }]
        };

        $scope.termValidationRules = {
            validationRules: [{
                type: "required",
                message: "Term is required"
            }]
        };

        $scope.amountValidationRules = {
            validationRules: [{
                type: "required",
                message: "Amount is required"
            }]
        };

        $scope.paidValidationRules = {
            validationRules: [{
                type: "required",
                message: "Paid is required"
            }]
        };

        $scope.balanceValidationRules = {
            validationRules: [{
                type: "required",
                message: "Balance is required"
            }]
        };

        $scope.submitButtonOptions = {
            text: 'Save',
            type: 'success',
            useSubmitBehavior: true
        };

        var validationGroup = "paymentMethod";
        $("#summaryPaymentMethod").dxValidationSummary({
            validationGroup: validationGroup
        });

        $scope.nameValidationRules = {
            validationRules: [{
                type: "required",
                message: "Name is required"
            }],
            validationGroup: validationGroup
        };

        $scope.accountNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "Account Name is required"
            }],
            validationGroup: validationGroup
        };

        $scope.payBillValidationRules = {
            validationRules: [{
                type: "required",
                message: "PayBill is required"
            }],
            validationGroup: validationGroup
        };

        $scope.tillNumberValidationRules = {
            validationRules: [{
                type: "required",
                message: "TillNumber is required"
            }],
            validationGroup: validationGroup
        };

        $scope.accountNumberValidationRules = {
            validationRules: [{
                type: "required",
                message: "AccountNumber is required"
            }],
            validationGroup: validationGroup
        };

        $scope.instructionValidationRules = {
            validationRules: [{
                type: "required",
                message: "Instruction is required"
            }],
            validationGroup: validationGroup
        };


        $scope.submitButtonOptionsPaymentMethod = {
            text: 'Save',
            type: 'success',
            useSubmitBehavior: true,
            validationGroup: validationGroup
        };

        $scope.clearData = function () {
            $scope.data = [];
        }



        $scope.onFormSubmitPaymentmethod = function (e) {
            $scope.data.SchoolId = $sessionStorage.SchoolId;
            $scope.pMData = {
                "SchoolId": $sessionStorage.SchoolId,
                "Name": $scope.data.Name,
                "AccountName": $scope.data.AccountName,
                "PayBill": $scope.data.PayBill,
                "TillNumber": $scope.data.TillNumber,
                "AccountNumber": $scope.data.AccountNumber,
                "Instruction": $scope.data.Instruction
            }
            $http.post(myUrl + "api/PaymentMethods", $scope.pMData).then(function (response) {
                $scope.paymentMethodData = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-addNewPaymentMethod').modal('hide');
                toaster.pop('success', 'Record Added', "Record successfully added");
            }, function (response) {

                toaster.pop('error', 'Error', "Could not add record. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
            });
            e.preventDefault();
        }

        $scope.onFormSubmitFee = function (e) {
            $scope.data.SchoolId = $sessionStorage.SchoolId;
            $scope.data.Year = new Date();
            $scope.feeData = {
                "StudentId": $scope.data.StudentId,
                "SchoolId": $sessionStorage.SchoolId,
                "PaymentMethodId": $scope.data.PaymentMethodId,
                "Year": $scope.data.Year,
                "Paid": $scope.data.Paid,
                "Term": $scope.data.Term,
                "Amount": $scope.data.Amount,
                "Balance": $scope.data.Balance
            }
            $http.post(myUrl + "api/Fees", $scope.feeData).then(function (response) {
                $scope.fees = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-addNewFee').modal('hide');
                toaster.pop('success', 'Record Added', "Record successfully added");
            }, function (response) {

                toaster.pop('error', 'Error', "Could not add record. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
            });
            e.preventDefault();
        }

        $scope.edit = function (data) {
            $scope.data =[];
            $scope.data.SchoolId = $sessionStorage.SchoolId;
            $scope.data.Year = new Date();
            $scope.feeeditid = data.ID;
            $scope.data.Term = data.Term;
            $scope.data.Amount = data.Amount;
            $scope.data.Paid = data.Paid;
            $scope.data.Balance = data.Balance;
        }

        $scope.saveEdit = function () {
            $scope.efeeData = {
                "StudentId": $scope.data.StudentId,
                "SchoolId": $sessionStorage.SchoolId,
                "PaymentMethodId": $scope.data.PaymentMethodId,
                "Year": $scope.data.Year,
                "Paid": $scope.data.Paid,
                "Term": $scope.data.Term,
                "Amount": $scope.data.Amount,
                "Balance": $scope.data.Balance
            }
            $http.put(myUrl + "api/Fees/" + $scope.feeeditid, $scope.efeeData).then(function (response) {
                $scope.fees = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-editFee').modal('hide');
                toaster.pop('success', 'Record Edited', "Record successfully edited");
            }, function (response) {

                toaster.pop('error', 'Error', "Could not edit record. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
            });


        }

        $scope.editPaymentMethod = function (data) {
            $scope.data = [];
            console.log(data);
            $scope.data.SchoolId = $sessionStorage.SchoolId;
            $scope.pmeditid = data.ID;
            $scope.data.Name = data.Name;
            $scope.data.AccountName = data.AccountName;
            $scope.data.PayBill = data.PayBill;
            $scope.data.TillNumber = data.TillNumber;
            $scope.data.AccountNumber = data.AccountNumber;
            $scope.data.Instruction = data.Instruction;
        }

        $scope.saveEditPaymentmethod = function () {
            $scope.epMData = {
                "SchoolId": $sessionStorage.SchoolId,
                "Name": $scope.data.Name,
                "AccountName": $scope.data.AccountName,
                "PayBill": $scope.data.PayBill,
                "TillNumber": $scope.data.TillNumber,
                "AccountNumber": $scope.data.AccountNumber,
                "Instruction": $scope.data.Instruction
            }
            $http.put(myUrl + "api/PaymentMethods/" + $scope.pmeditid, $scope.epMData).then(function (response) {
                $scope.paymentMethodData = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-editPaymentMethod').modal('hide');
                toaster.pop('success', 'Record Edited', "Record successfully edited");
            }, function (response) {

                toaster.pop('error', 'Error', "Could not edit record. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
            });


        }



    }]);