/**
 * Created by clintonb on 4/23/2017.
 */
'use strict';

angular.module('ElimuShare.students', ['ngRoute', 'ngStorage'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/students', {
            templateUrl: 'students/students.html',
            controller: 'StudentsCtrl'
        });
    }])

    .controller('StudentsCtrl', ['$scope', 'toaster', '$http', '$location', '$sessionStorage', 'myUrl', function ($scope, toaster, $http, $location, $sessionStorage, myUrl) {
        $scope.baseurl = myUrl;
        if ($sessionStorage.UserId != null && $sessionStorage.SchoolId != null) {
            $http.get(myUrl + "api/SchoolByAdministratorId?AdministratorId=" + $sessionStorage.UserId).then(function (response) {
                var results = response.data;

                $scope.LogoUrl = results.LogoUrl;
                $scope.Name = results.Name;
                $sessionStorage.SchoolId = results.ID;
                $sessionStorage.LogoUrl = $scope.LogoUrl;
                $sessionStorage.Name = $scope.Name;
                $sessionStorage.SchoolType = results.SchoolType;
                console.log($sessionStorage.SchoolType);

                if($sessionStorage.SchoolType.Category === "Secondary"){
                    $scope.schoolType = {
                        classLevelLookup: {
                            items: ["Form 1", "Form 2", "Form 3", "Form 4"],
                            searchEnabled: true,
                            placeholder: "  [Select One]",
                            visible: true,
                            title: "Class Level",
                            showPopupTitle: true
                        }
                    }
                } else if($sessionStorage.SchoolType.Category === "Primary"){
                    $scope.schoolType = {
                        classLevelLookup: {
                            items: ["Class 1", "Class 2", "Class 3", "Class 4", "Class 5", "Class 6", "Class 7", "Class 8"],
                            searchEnabled: true,
                            placeholder: "  [Select One]",
                            visible: true,
                            title: "Class Level",
                            showPopupTitle: true
                        }
                    }
                } else if($sessionStorage.SchoolType.Category === "Kindergarten"){
                    $scope.schoolType = {
                        classLevelLookup: {
                            items: ["Baby Class", "Nursery", "Pre Unit"],
                            searchEnabled: true,
                            placeholder: "  [Select One]",
                            visible: true,
                            title: "Class Level",
                            showPopupTitle: true
                        }
                    }
                }

                $scope.loadPageFunctions();
            }, function (response) {
                toaster.pop('error', 'Error', "Could not load data, check your internet connectivity");
            });
        } else {
            $location.path('/login');
        }

        var validationGroup = "student";


        $scope.checked = {
            value: false,
            onValueChanged: function (e) {
                DevExpress.validationEngine.resetGroup(validationGroup);
                if( $scope.newParent == false){
                    DevExpress.validationEngine.resetGroup(validationGroup);
                    $scope.selectParentValidationRules = {
                        validationRules: [{
                            type: "required",
                            message: "Parent is required"
                        }],

                    };

                } else{
                    DevExpress.validationEngine.resetGroup(validationGroup);
                    $scope.selectParentValidationRules = {};
                    $scope.parentFirstNameValidationRules = {
                        validationRules: [{
                            type: "required",
                            message: "parent first name is required"
                        }],

                    };

                    $scope.parentSecondNameValidationRules = {
                        validationRules: [{
                            type: "required",
                            message: "Parent second name is required"
                        }],

                    };

                    $scope.parentSurNameValidationRules = {
                        validationRules: [{
                            type: "required",
                            message: "Parent sur name is required"
                        }],

                    };

                    $scope.parentMobileNumberValidationRules = {
                        validationRules: [{
                            type: "required",
                            message: "Mobile number is required"
                        }],

                    };
                }

            }
        };
        $scope.newParent = false;

        $scope.clearData = function () {
            $scope.data = [];
        }



        $scope.loadPageFunctions = function () {
            $http.get(myUrl + "api/DxStudentsBySchoolId?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {
                var results = response.data;

                $scope.students = response.data;

            }, function (response) {
                if(response.status == 404){
                    toaster.pop('info', 'Info', "No students found, please add a student");
                }else if(response.status == 500){
                    toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                } else{
                    toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                }
            });

            $http.get(myUrl + "api/ParentsBySchoolId?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {
                var results = response.data;
                $scope.parentData = results;
                $scope.addStudent = {
                    parentLookup: {
                        bindingOptions:{
                            disabled: 'switchValue'
                        },
                        dataSource: $scope.parentData,
                        valueExpr:"ID",
                        displayExpr:"SurName",
                        searchEnabled: true,
                        placeholder: "  [Select One]",
                        visible: true,
                        title: "Parents",
                        itemTemplate: function (data, index, container) {
                            var row = $("<div>").addClass("row-fluid");
                            $("<div>").addClass("col-xs-3").text(data["FirstName"]).appendTo(row);
                            $("<div>").addClass("col-xs-3").text(data["SecondName"]).appendTo(row);
                            $("<div>").addClass("col-xs-3").text(data["SurName"]).appendTo(row);
                            container.append(row);

                        },
                        showPopupTitle: true
                    }
                }

            }, function (response) {
                if(response.status == 404){
                    toaster.pop('info', 'Error', "No parent found, please add a parent");
                }else if(response.status == 500){
                    toaster.pop('error', 'Info', "Something went wrong please try again, if the issue persists contact system admin");
                } else{
                    toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                }

            });
        }






        $scope.sort = function(keyname){
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }

        $scope.parentForm = {
            FirstName: {
                placeholder: "First Name",
                value: ""
            },
            SecondName: {
                placeholder: "Second Name",
                value: ""
            },
            SurName: {
                placeholder: "Sur Name",
                value: ""
            },
            ClassLevel: {
                placeholder: "Class Level",
                value: ""
            },
            ParentFirstName: {
                bindingOptions:{
                    disabled: 'switchValue'
                },
                placeholder: "Parent First Name",
                value: ""
            },
            ParentSecondName: {
                bindingOptions:{
                    disabled: 'switchValue'
                },
                placeholder: "Parent Second Name",
                value: ""
            },
            ParentSurName: {
                bindingOptions:{
                    disabled: 'switchValue'
                },
                placeholder: "Parent Sur Name",
                value: ""
            },
            ParentMobileNumberDirtyB: {
                bindingOptions:{
                    disabled: 'switchValue'
                },
                placeholder: "Parent Mobile Number",
                value: ""
            },
            ParentMobileNumberDirty: {
                bindingOptions:{
                    disabled: 'switchValue'
                },
                placeholder: "Parent Mobile Number",
                value: ""
            }
        }

        $scope.delete = function (data) {
            $scope.StudentDeleteItem = data;

        }



        $scope.saveDelete = function () {
            $http.delete(myUrl + "api/Students/" + $scope.StudentDeleteItem.ID).then(function (response) {
                $scope.students = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-deleteStudent').modal('hide');
                toaster.pop('success', 'Success', "Student Record Deleted");
            }, function (response) {
                if(response.status == 404){
                    toaster.pop('info', 'Error', "No data found");
                }else if(response.status == 500){
                    toaster.pop('error', 'Info', "Something went wrong please try again, if the issue persists contact system admin");
                } else{
                    toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                }

            });
        }

        if( $scope.newParent == false){

            $scope.selectParentValidationRules = {
                validationRules: [{
                    type: "required",
                    message: "Parent is required"
                }],

            };

        } else{


            $scope.parentFirstNameValidationRules = {
                validationRules: [{
                    type: "required",
                    message: "parent first name is required"
                }],

            };

            $scope.parentSecondNameValidationRules = {
                validationRules: [{
                    type: "required",
                    message: "Parent second name is required"
                }],

            };

            $scope.parentSurNameValidationRules = {
                validationRules: [{
                    type: "required",
                    message: "Parent sur name is required"
                }],

            };

            $scope.parentMobileNumberValidationRules = {
                validationRules: [{
                    type: "required",
                    message: "Mobile number is required"
                }],

            };
        }




        $("#summary").dxValidationSummary({
            validationGroup: validationGroup
        });



        $scope.firstNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "First name is required"
            }],
            validationGroup: validationGroup
        };

        $scope.secondNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "Second name is required"
            }],
            validationGroup: validationGroup
        };

        $scope.surNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "Sur name is required"
            }],
            validationGroup: validationGroup
        };

        $scope.classLevelValidationRules = {
            validationRules: [{
                type: "required",
                message: "Class level is required"
            }],
            validationGroup: validationGroup
        };

        $scope.imageValidationRules = {
            validationRules: [{
                type: "required",
                message: "Image is required"
            }],
            validationGroup: validationGroup
        };

        $scope.submitButtonOptionsAdd = {
            text: 'Save',
            type: 'success',
            useSubmitBehavior: true,
            validationGroup: validationGroup
        };



        $scope.onFormSubmitAdd = function (e) {
            $scope.data.ImgUrl = myUrl + "images/schoolLogos/" + $scope.data.ImgUrl[0].name;
            if($scope.newParent == true){
                $scope.data.MobileNumberDirty = $scope.data.ParentMobileNumberDirtyB;
                $scope.parentForm.ParentMobileNumberDirty.value = $scope.data.MobileNumberDirtyB;
                if ($scope.data.MobileNumberDirty.charAt(0) === "0") {
                    $scope.data.MobileNumberDirty = "+254" + $scope.data.MobileNumberDirty.substr(1);
                } else if ($scope.data.MobileNumberDirty.charAt(0) === "7") {
                    $scope.data.MobileNumberDirty = "+254" + $scope.data.MobileNumberDirty;
                } else if ($scope.data.MobileNumberDirty.charAt(0) === "2") {
                    $scope.user.MobileNumberDirty = "+" + $scope.data.MobileNumberDirty;
                } else if ($scope.data.MobileNumberDirty.charAt(0) === "+") {
                    //Do nothing.
                }
                $scope.data.MobileNumber = $scope.data.MobileNumberDirty;
                $scope.parentData = {
                    FirstName: $scope.data.ParentFirstName,
                    SecondName: $scope.data.ParentSecondName,
                    SurName: $scope.data.ParentSurName,
                    MobileNumber: $scope.data.MobileNumber,
                    Password: "1234",
                    Gender: "Not Set"
                }
                $http.post(myUrl + "api/ParentsBySchool", $scope.parentData).then(function (response) {
                    var results = response.data;
                    $scope.studentData = {
                        SchoolId: $sessionStorage.SchoolId,
                        ParentId: results.ID,
                        FirstName: $scope.data.FirstName,
                        SecondName: $scope.data.SecondName,
                        SurName: $scope.data.SurName,
                        ImgUrl: $scope.data.ImgUrl,
                        ClassLevel: $scope.data.ClassLevel
                    }
                    $http.post(myUrl + "api/Students", $scope.studentData).then(function (response) {
                        $scope.students = [];
                        $scope.loadPageFunctions();
                        $('#con-close-modal-addNewStudent').modal('hide');
                        toaster.pop('success', 'Success', "Student Record Added");
                    }, function (response) {
                        if(response.status == 404){
                            toaster.pop('info', 'Error', "No data found");
                        }else if(response.status == 500){
                            toaster.pop('error', 'Info', "Something went wrong please try again, if the issue persists contact system admin");
                        } else{
                            toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                        }

                    });
                }, function (response) {
                    if(response.status == 404){
                        toaster.pop('info', 'Error', "No data found");
                    }else if(response.status == 500){
                        toaster.pop('error', 'Info', "Something went wrong please try again, if the issue persists contact system admin");
                    } else{
                        toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                    }

                });
            }else {
                $scope.studentData = {
                    SchoolId: $sessionStorage.SchoolId,
                    ParentId: $scope.data.ParentId,
                    FirstName: $scope.data.FirstName,
                    SecondName: $scope.data.SecondName,
                    SurName: $scope.data.SurName,
                    ImgUrl: $scope.data.ImgUrl,
                    ClassLevel: $scope.data.ClassLevel
                }

                $http.post(myUrl + "api/Students", $scope.studentData).then(function (response) {
                    $scope.students = [];
                    $scope.loadPageFunctions();
                    $scope.data.ParentId == null;
                    $('#con-close-modal-addNewStudent').modal('hide');
                    toaster.pop('success', 'Success', "Student Record Added");
                }, function (response) {
                    if(response.status == 404){
                        toaster.pop('info', 'Error', "No data found");
                    }else if(response.status == 500){
                        toaster.pop('error', 'Info', "Something went wrong please try again, if the issue persists contact system admin");
                    } else{
                        toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                    }

                });
            }

        }

        $scope.edit = function (data) {
            $scope.data = [];
            $scope.studenteditid = data.ID;
            $scope.data.FirstName = data.FirstName;
            $scope.data.SecondName = data.SecondName;
            $scope.data.SurName = data.SurName;
            $scope.data.ClassLevel = data.ClassLevel;
        }

        $scope.saveEdit = function () {
            $scope.data.ImgUrl = myUrl + "images/schoolLogos/" + $scope.data.ImgUrl[0].name;
            $scope.studentData = {
                SchoolId: $sessionStorage.SchoolId,
                ParentId: $scope.data.ParentId,
                FirstName: $scope.data.FirstName,
                SecondName: $scope.data.SecondName,
                SurName: $scope.data.SurName,
                ImgUrl: $scope.data.ImgUrl,
                ClassLevel: $scope.data.ClassLevel
            }
            $http.put(myUrl + "api/Students/" + $scope.studenteditid, $scope.studentData).then(function (response) {
                $scope.students = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-editStudent').modal('hide');
                toaster.pop('success', 'Success', "Student Record Added");
            }, function (response) {
                if(response.status == 404){
                    toaster.pop('info', 'Error', "No data found");
                }else if(response.status == 500){
                    toaster.pop('error', 'Info', "Something went wrong please try again, if the issue persists contact system admin");
                } else{
                    toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                }

            });


        }





    }]);