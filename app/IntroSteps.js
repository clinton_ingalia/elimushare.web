/**
 * Created by clintonb on 5/12/2017.
 */
function startIntro(){
    var intro = introJs();
    intro.setOption('showProgress', true);
    intro.setOptions({
        steps: [
            {
                intro: "Welcome to ElimuShare"
            },
            {
                element: document.querySelector('#step1'),
                intro: "View School General Performance"
            },
            {
                element: document.querySelectorAll('#step2')[0],
                intro: "Have a View of Subject Performance",
                position: 'right'
            },
            {
                element: '#step3',
                intro: 'School Fee Payment Summary',
                position: 'left'
            },
            {
                element: '#step4',
                intro: "General School Statistics",
                position: 'bottom'
            },
            {
                element: '#step5',
                intro: 'Latest information'
            }
        ]
    });
    intro.start();
}