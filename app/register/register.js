/**
 * Created by clintonb on 4/13/2017.
 */
'use strict';

angular.module('ElimuShare.register', ['ngRoute', 'toaster', 'ngAnimate'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/register', {
            templateUrl: 'register/register.html',
            controller: 'RegisterCtrl'
        });
    }])

    .controller('RegisterCtrl', ['$scope', 'toaster', '$http', '$location', 'WizardHandler', 'myUrl', function($scope, toaster, $http, $location, WizardHandler, myUrl) {
        $scope.baseurl = myUrl;
        $scope.user = {
            FirstName:"",
            SecondName:"",
            SurName:"",
            MobileNumber:"",
            MobileNumberDirtyB:"",
            MobileNumberDirty:"",
            Password:"",
            ConfirmPassword:"",
            Gender:"",
            UserRoleId:"52f7a28a-033a-42a0-a35f-e9ced4f3fbdb"
        };

        $scope.school = {
            Name:"",
            AdministratorId: "",
            LogoUrl:"",
            SchoolCategory:"",
            Boarding:"",
            SchoolGender:"",
            Bio:"",
            Mission:"",
            Vission:"",
            Motto:"",
            Phone:"",
            PhoneDirtyB:"",
            PhoneDirty:"",
            Email:""
        };

        $scope.logoObject = {
            logo:""
        }
        var gender = ['Male', 'Female', 'Other'];
        var genderSchools = ['Male', 'Female', 'Mixed'];
        var schoolCategories = ['Kindergarten', 'Primary', 'Secondary'];
        $scope.Gender = gender;
        $scope.GenderSchools = genderSchools;
        $scope.SchoolCategories = schoolCategories;

        $scope.firstNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "First name is required"
            }]
        };

        $scope.missionValidationRules = {
            validationRules: [{
                type: "required",
                message: "Mission is required"
            }]
        };

        $scope.visionValidationRules = {
            validationRules: [{
                type: "required",
                message: "Vision is required"
            }]
        };

        $scope.mottoValidationRules = {
            validationRules: [{
                type: "required",
                message: "Motto is required"
            }]
        };

        $scope.schoolNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "School name is required"
            }]
        };

        $scope.bioValidationRules = {
            validationRules: [{
                type: "required",
                message: "School Bio is required"
            }]
        };

        $scope.secondNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "Second name is required"
            }]
        };

        $scope.surNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "SurName is required"
            }]
        };

        $scope.mobileNumberValidationRules = {
            validationRules: [{
                type: "required",
                message: "Mobile number is required"
            }]
        };

        $scope.passwordValidationRules = {
            validationRules: [{
                type: "required",
                message: "Password is required"
            }]
        };

        $scope.confirmPasswordValidationRules = {
            validationRules: [{
                type: "compare",
                comparisonTarget: function () {
                    var password = $("#password-validation").dxTextBox("instance");
                    if (password) {
                        return password.option("value");
                    }
                },
                message: "'Password' and 'Confirm Password' do not match."
            },
                {
                    type: "required",
                    message: "Confirm Password is required"
                }]
        };

        $scope.genderValidationRules = {
            validationRules: [{
                type: "required",
                message: "Gender is required"
            }]
        };

        $scope.schoolCategoryValidationRules = {
            validationRules: [{
                type: "required",
                message: "School category is required"
            }]
        };

        $scope.boardingValidationRules = {
            validationRules: [{
                type: "required",
                message: "Boarding is required"
            }]
        };

        $scope.emailValidationRules = {
            validationRules: [{
                type: "required",
                message: "Email is required"
            }]
        };

        $scope.checkValidationRules = {
            validationRules: [{
                type: "compare",
                comparisonTarget: function(){ return true; },
                message: "You must agree to the Terms and Conditions"
            }]
        };

        $scope.adminRegisterInput = {
            FirstName: {
                placeholder: "Enter First Name",
                showClearButton: true
            },
            SecondName: {
                placeholder: "Enter Second Name",
                showClearButton: true
            },
            SurName: {
                placeholder: "Enter SurName",
                showClearButton: true
            },
            password: {
                mode: "password",
                placeholder: "Enter password",
                showClearButton: true
            },
            mobileNumber: {
                mode: "tel",
                placeholder: "Enter mobile number 0712 345 678",
                showClearButton: true
            },
        };

        $scope.schoolRegisterInput = {
            Name: {
                placeholder: "Enter School Name",
                showClearButton: true
            },
            SecondName: {
                placeholder: "Enter Second Name",
                showClearButton: true
            },
            SurName: {
                placeholder: "Enter SurName",
                showClearButton: true
            },
            password: {
                mode: "password",
                placeholder: "Enter password",
                showClearButton: true
            },
            mobileNumber: {
                mode: "tel",
                placeholder: "Enter mobile number 0712 345 678",
                showClearButton: true
            },
            bio: {
                placeholder: "Enter School Bio",
                showClearButton: true
            },
            Mission: {
                placeholder: "Enter School Mission",
                showClearButton: true
            },
            Vission: {
                placeholder: "Enter School Vision",
                showClearButton: true
            },
            Motto: {
                placeholder: "Enter School Motto",
                showClearButton: true
            },
            Email: {
                placeholder: "Enter School Email",
                showClearButton: true
            },
        };

        $scope.submitButtonOptions = {
            text: 'Register',
            type: 'success',
            useSubmitBehavior: true
        };

        $scope.submitSchoolButtonOptions = {
            text: 'Save',
            type: 'success',
            useSubmitBehavior: true
        };


        $scope.onFormSubmit = function(e) {
            $scope.user.MobileNumberDirty = $scope.user.MobileNumberDirtyB;
            $scope.adminRegisterInput.mobileNumber.value = $scope.user.MobileNumberDirtyB;
            if($scope.user.MobileNumberDirty.charAt(0) === "0"){
                $scope.user.MobileNumberDirty = "+254" + $scope.user.MobileNumberDirty.substr(1);
            }else if($scope.user.MobileNumberDirty.charAt(0) === "7"){
                $scope.user.MobileNumberDirty = "+254" + $scope.user.MobileNumberDirty;
            }else if ($scope.user.MobileNumberDirty.charAt(0) === "2") {
                $scope.user.MobileNumberDirty = "+" + $scope.user.MobileNumberDirty;
            }else if ($scope.user.MobileNumberDirty.charAt(0) === "+") {
                //Do nothing.
            }
            $scope.user.MobileNumber = $scope.user.MobileNumberDirty;
            if($scope.user.SecondName === ""){
                $scope.user.SecondName = "Null";
            }
            $http.post(myUrl + "api/Users", $scope.user).
            then(function(response) {
                WizardHandler.wizard().next();
                $scope.school.AdministratorId = response.data.ID;
                console.log($scope.school);
                toaster.pop('success', "Registration Successfull", "Welcome to ElimuShare, please proceed to school registration");
            }, function(response) {

                if(response.statusCode == 409){
                    toaster.pop('error', data, "User already exists");
                } else {
                    toaster.pop('error', "An error occurred", "Please try again");
                }
            });

            e.preventDefault();
        };

        $scope.onFormSubmitSchool = function(e) {
            $scope.school.PhoneDirty = $scope.school.PhoneDirtyB;
            $scope.schoolRegisterInput.mobileNumber.value = $scope.school.PhoneDirtyB;
            if($scope.school.PhoneDirty.charAt(0) === "0"){
                $scope.school.PhoneDirty = "+254" + $scope.school.PhoneDirty.substr(1);
            }else if($scope.school.PhoneDirty.charAt(0) === "7"){
                $scope.school.PhoneDirty = "+254" + $scope.school.PhoneDirty;
            }else if ($scope.school.PhoneDirty.charAt(0) === "2") {
                $scope.school.PhoneDirty = "+" + $scope.school.PhoneDirty;
            }else if ($scope.school.PhoneDirty.charAt(0) === "+") {
                //Do nothing.
            }
            $scope.school.Phone = $scope.school.PhoneDirty;
            $scope.school.LogoUrl = myUrl + "images/schoolLogos/" + $scope.logoObject.logo[0].name;
            $http.post(myUrl + "api/Schools", $scope.school).
            then(function(response) {
                WizardHandler.wizard().next();
                $location.path('/login');
                toaster.pop('success', "Registration Successfull", "Please proceed to login");
            }, function(response) {
                if(response.statusCode == 409){
                    toaster.pop('error', data, "School already exists");
                } else {
                    toaster.pop('error', "An error occurred", "Please try again");
                }
            });
            e.preventDefault();
        };




    }]);