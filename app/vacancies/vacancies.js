/**
 * Created by clintonb on 4/23/2017.
 */
'use strict';

angular.module('ElimuShare.vacancies', ['ngRoute', 'ngStorage'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/vacancies', {
            templateUrl: 'vacancies/vacancies.html',
            controller: 'VacanciesCtrl'
        });
    }])

    .controller('VacanciesCtrl', ['$scope', 'toaster', '$http', '$location', '$sessionStorage', 'myUrl', function ($scope, toaster, $http, $location, $sessionStorage, myUrl) {
        if ($sessionStorage.UserId != null && $sessionStorage.SchoolId != null) {
            $http.get(myUrl + "api/SchoolByAdministratorId?AdministratorId=" + $sessionStorage.UserId).then(function (response) {
                var results = response.data;

                $scope.LogoUrl = results.LogoUrl;
                $scope.Name = results.Name;
                $sessionStorage.SchoolId = results.ID;
                $sessionStorage.LogoUrl = $scope.LogoUrl;
                $sessionStorage.Name = $scope.Name;
                $scope.loadPageFunctions();

            }, function (response) {
                toaster.pop('error', 'Error', "Could not load data, check your internet connectivity");
            });
        } else {
            $location.path('/login');
        }

        $scope.loadPageFunctions = function () {
            $http.get(myUrl + "api/VacanciesBySchoolId?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {
                var results = response.data;
                $scope.vacancies = results;
            }, function (response) {
                toaster.pop('error', 'Error', "Could not load data, if the issue persists contact system admin");
            });
        }

        $scope.delete = function (data) {
            $scope.vacancyDeleteId = data.ID;
        }

        $scope.saveDelete = function (data) {
            $http.delete(myUrl + "api/Vacancies/" + $scope.vacancyDeleteId).then(function (response) {
                $scope.vacancies = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-deleteVacancy').modal('hide');
                toaster.pop('success', 'Record Deleted', "Record successfully deleted");
            }, function (response) {

                toaster.pop('error', 'Error', "Could not delete record. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
            });
        }

        $scope.titleValidationRules = {
            validationRules: [{
                type: "required",
                message: "Title is required"
            }]
        };

        $scope.descriptionValidationRules = {
            validationRules: [{
                type: "required",
                message: "Description is required"
            }]
        };

        $scope.numberValidationRules = {
            validationRules: [{
                type: "required",
                message: "Number is required"
            }]
        };

        $scope.submitButtonOptions = {
            text: 'Save',
            type: 'success',
            useSubmitBehavior: true
        };

        $scope.onFormSubmitVacancy = function (e) {
            $scope.data.SchoolId = $sessionStorage.SchoolId;
            $scope.vacancyData = {
                "SchoolId": $sessionStorage.SchoolId,
                "Name": $scope.data.Name,
                "Title": $scope.data.Title,
                "Description": $scope.data.Description,
                "Number": $scope.data.Number
            }
            $http.post(myUrl + "api/Vacancies", $scope.vacancyData).then(function (response) {
                $scope.vacancies = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-addNewVacancy').modal('hide');
                toaster.pop('success', 'Record Added', "Record successfully added");
            }, function (response) {

                toaster.pop('error', 'Error', "Could not add record. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
            });
            e.preventDefault();
        }

        $scope.edit = function (data) {
            $scope.data = [];
            $scope.data.SchoolId = $sessionStorage.SchoolId;
            $scope.vacancyeditid = data.ID;
            $scope.data.Title = data.Title;
            $scope.data.Description = data.Description;
            $scope.data.Number = data.Number;

        }

        $scope.clearData = function () {
            $scope.data = [];
        }

        $scope.saveEdit = function () {
            $scope.newData = {
                "SchoolId": $scope.data.SchoolId,
                "Name": $scope.data.Name,
                "Title": $scope.data.Title,
                "Description": $scope.data.Description,
                "Number": $scope.data.Number
            }
            $http.put(myUrl + "api/Vacancies/" + $scope.vacancyeditid, $scope.newData).then(function (response) {
                $scope.vacancies = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-editVacancy').modal('hide');
                toaster.pop('success', 'Record Edited', "Record successfully edited");
            }, function (response) {

                toaster.pop('error', 'Error', "Could not edit record. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
            });


        }



    }]);