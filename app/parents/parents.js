/**
 * Created by clintonb on 4/23/2017.
 */
'use strict';

angular.module('ElimuShare.parents', ['ngRoute', 'ngStorage'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/parents', {
            templateUrl: 'parents/parents.html',
            controller: 'ParentsCtrl'
        });
    }])

    .controller('ParentsCtrl', ['$scope', 'toaster', '$http', '$location', '$sessionStorage', 'myUrl', function ($scope, toaster, $http, $location, $sessionStorage, myUrl) {
        if ($sessionStorage.UserId != null && $sessionStorage.SchoolId != null) {
            $http.get(myUrl + "api/SchoolByAdministratorId?AdministratorId=" + $sessionStorage.UserId).then(function (response) {
                var results = response.data;

                $scope.LogoUrl = results.LogoUrl;
                $scope.Name = results.Name;
                $sessionStorage.SchoolId = results.ID;
                $sessionStorage.LogoUrl = $scope.LogoUrl;
                $sessionStorage.Name = $scope.Name;

                $scope.loadPageFunctions();
            }, function (response) {
                toaster.pop('error', 'Error', "Could not load data, check your internet connectivity");
            });
        } else {
            $location.path('/login');
        }

        $scope.loadPageFunctions = function () {


            $http.get(myUrl + "api/ParentsBySchoolId?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {

                $scope.parents = response.data;


            }, function (response) {
                if (response.status == 404) {
                    toaster.pop('info', 'Info', "No Parents found, please add a parent");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                }
            });
        }

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        $scope.parentForm = {
            FirstName: {
                placeholder: "First Name",
                value: ""
            },
            SecondName: {
                placeholder: "Second Name",
                value: ""
            },
            SurName: {
                placeholder: "Sur Name",
                value: ""
            },
            MobileNumber: {
                placeholder: "Mobile Number 0712 345 678",
                value: ""
            }
        }


        $scope.delete = function (data) {
            $scope.ParentDeleteItem = data;
        }

        $scope.saveDelete = function () {
            $http.delete(myUrl + "api/Parents/" + $scope.ParentDeleteItem.ID).then(function (response) {
                $scope.parents = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-deleteParent').modal('hide');
                toaster.pop('success', 'Success', "Parent Record Deleted");
            }, function (response) {
                if (response.status == 404) {
                    toaster.pop('info', 'Error', "No data found");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Something went wrong please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                }

            });
        }

        $scope.firstNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "First name is required"
            }]
        };

        $scope.secondNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "Second name is required"
            }]
        };

        $scope.surNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "Sur name is required"
            }]
        };

        $scope.mobileNumberValidationRules = {
            validationRules: [{
                type: "required",
                message: "mobile number is required"
            }]
        };

        $scope.genderValidationRules = {
            validationRules: [{
                type: "required",
                message: "Gender is required"
            }]
        };

        $scope.submitButtonOptionsAdd = {
            text: 'Save',
            type: 'success',
            useSubmitBehavior: true
        };

        $scope.submitButtonOptionsEdit = {
            text: 'Save',
            type: 'success',
            useSubmitBehavior: true
        };

        $scope.onFormSubmitAdd = function (e) {

            $scope.data.MobileNumberDirty = $scope.data.MobileNumberDirtyB;
            $scope.parentForm.MobileNumber.value = $scope.data.MobileNumberDirtyB;
            if ($scope.data.MobileNumberDirty.charAt(0) === "0") {
                $scope.data.MobileNumberDirty = "+254" + $scope.data.MobileNumberDirty.substr(1);
            } else if ($scope.data.MobileNumberDirty.charAt(0) === "7") {
                $scope.data.MobileNumberDirty = "+254" + $scope.data.MobileNumberDirty;
            } else if ($scope.data.MobileNumberDirty.charAt(0) === "2") {
                $scope.user.MobileNumberDirty = "+" + $scope.data.MobileNumberDirty;
            } else if ($scope.data.MobileNumberDirty.charAt(0) === "+") {
                //Do nothing.
            }
            $scope.data.MobileNumber = $scope.data.MobileNumberDirty;
            $scope.parentData = {
                FirstName: $scope.data.FirstName,
                SecondName: $scope.data.SecondName,
                SurName: $scope.data.SurName,
                MobileNumber: $scope.data.MobileNumber,
                Password: "1234",
                Gender: "Not Set"
            }


            $http.post(myUrl + "api/Parents", $scope.parentData).then(function (response) {
                $scope.parents = new Array();
                $scope.parents.push($scope.parentData);

                $('#con-close-modal-addNewParent').modal('hide');
                toaster.pop('success', 'Success', "Parent Record Added");
            }, function (response) {
                if (response.status == 409) {
                    toaster.pop('error', 'Error', "Parent Already Exists");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Something went wrong please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                }

            });
            e.preventDefault();
        }


        $scope.edit = function (data) {
            $scope.data = {
                FirstName: ""
            }
            console.log(data);
            $scope.parenteditid = data.ID;
            //$scope.parentFormEdit.$setPristine();
            $scope.data.FirstName = data.FirstName;
            $scope.data.SecondName = data.SecondName;
            $scope.data.SurName = data.SurName;
            $scope.data.MobileNumberDirtyB = data.MobileNumber;
            $scope.data.Gender = data.Gender;
            $scope.parentFormEdit = {
                FirstName: {
                    placeholder: data.FirstName
                },
                SecondName: {
                    placeholder: data.SecondName
                },
                SurName: {
                    placeholder: data.SurName
                },
                MobileNumber: {
                    placeholder: data.MobileNumber
                },
                Gender: {
                    placeholder: data.Gender
                },
            }

        }

        $scope.Gender = {
            Type: {
                items: ["Male", "Female", "Other"],
                searchEnabled: true,
                placeholder: "  [Select One]",
                visible: true,
                title: "Gender",
                showPopupTitle: true
            }
        }

        $scope.saveEditChsnges = function () {
            $scope.data.MobileNumberDirty = $scope.data.MobileNumberDirtyB;
            $scope.parentForm.MobileNumber.value = $scope.data.MobileNumberDirtyB;

            $scope.data.MobileNumber = $scope.data.MobileNumberDirtyB;
            $scope.parentData = {
                FirstName: $scope.data.FirstName,
                SecondName: $scope.data.SecondName,
                SurName: $scope.data.SurName,
                MobileNumber: $scope.data.MobileNumber,
                Gender: $scope.data.Gender
            }
            $http.put(myUrl + "api/EditParentBySchool?id=" + $scope.parenteditid, $scope.parentData).then(function (response) {
                $scope.parents = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-editParent').modal('hide');
                toaster.pop('success', 'Success', "Parent Record Edited");
            }, function (response) {
                if (response.status == 404) {
                    toaster.pop('info', 'Error', "No data found");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Something went wrong please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
                }

            });


        }

        $http.get(myUrl + "api/Parents").then(function (response) {
            var results = response.data;
            $scope.parentData = results;
            $scope.addStudent = {
                parentLookup: {
                    dataSource: $scope.parentData,
                    valueExpr: "ID",
                    displayExpr: "SurName",
                    searchEnabled: true,
                    placeholder: "  [Select One]",
                    visible: true,
                    title: "Parents",
                    itemTemplate: function (data, index, container) {
                        var row = $("<div>").addClass("row-fluid");
                        $("<div>").addClass("col-xs-3").text(data["FirstName"]).appendTo(row);
                        $("<div>").addClass("col-xs-3").text(data["SecondName"]).appendTo(row);
                        $("<div>").addClass("col-xs-3").text(data["SurName"]).appendTo(row);
                        container.append(row);

                    },
                    showPopupTitle: true
                }
            }

        }, function (response) {
            if (response.status == 404) {
                toaster.pop('info', 'Info', "No data found");
            } else if (response.status == 500) {
                toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
            } else {
                toaster.pop('error', 'Error', "Something went wrong please try again, if the issue persists contact system admin");
            }
        });


    }]);