/**
 * Created by clintonb on 4/23/2017.
 */
'use strict';

angular.module('ElimuShare.profile', ['ngRoute', 'ngStorage'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/profile', {
            templateUrl: 'profile/profile.html',
            controller: 'ProfileCtrl'
        });
    }])

    .controller('ProfileCtrl', ['$scope', 'toaster', '$http', '$location', '$sessionStorage', 'myUrl', function ($scope, toaster, $http, $location, $sessionStorage, myUrl) {
        $scope.baseurl = myUrl;
        $scope.schoolData = {
            Name: "School Name",
            Email: "Email",
            LogoUrl: "Logo Url",
            County: "County Name",
            LocationName: "Location",
            Boarding: true,
            Gender: "Gender",
            Category: "Category",
            Motto: "Motto",
            Vision: "Vision",
            Mission: "Mission",
            Bio: "Bio",
            Address: "School Address",
            PostalCode: "Postal Code",
            Phone: "+254700000000"
        };
        if ($sessionStorage.UserId != null) {
            $http.get(myUrl + "api/SchoolByAdministratorId?AdministratorId=" + $sessionStorage.UserId).then(function (response) {
                var results = response.data;
                console.log(response.data);
                $scope.LogoUrl = results.LogoUrl;
                $scope.Name = results.Name;
                $scope.Bio = results.Bio;
                $sessionStorage.SchoolId = results.ID;
                $sessionStorage.LogoUrl = $scope.LogoUrl;
                $sessionStorage.Name = $scope.Name;
                //load form data

                $scope.data = [];
                $scope.data.County = [];
                $scope.data.Location = [];
                $scope.data.SchoolType = [];
                $scope.data.Name = results.Name;
                $scope.data.Email = results.Email;
                $scope.data.LogoUrl = results.LogoUrl;
                $scope.data.County.Name = results.County.Name;
                $scope.data.Location.Name = results.Location.Name;
                $scope.data.SchoolType.Boarding = results.SchoolType.Boarding;
                $scope.data.SchoolType.Category = results.SchoolType.Category;
                $scope.data.SchoolType.Gender = results.SchoolType.Gender;
                $scope.data.Motto = results.Motto;
                $scope.data.Vission = results.Vission;
                $scope.data.Mission = results.Mission;
                $scope.data.Bio = results.Bio;
                $scope.data.Address = results.AddressLine;
                $scope.data.PostalCode = results.PostalCode;
                $scope.data.Phone = results.Phone;
                $scope.ID = results.ID;

                //enable editing
                $scope.updateBtn = false;
            }, function (response) {
                //toaster.pop('error', response.data, "Check mobile number or password");
            });
        } else {
            $location.path('/login');
        }

        $scope.parentForm = {
            Name: {
                value: 'data.Name'
            },
            Email: {
                value: 'data.Email'
            },
            County: {
                value: 'data.County.Name'
            },
            Location: {
                value: 'data.Location.Name'
            },
            Boarding: {
                value: 'data.SchoolType.Boarding'
            },
            Motto: {
                value: 'data.Motto'
            },
            Vission: {
                value: 'data.Vission'
            },
            Mission: {
                value: 'data.Mission'
            },
            Bio: {
                value: 'data.Bio'
            },
            Address: {
                value: 'data.Address'
            },
            PostalCode: {
                value: 'data.PostalCode'
            },
            Phone: {
                value: 'data.Phone'
            }
        }

        $scope.SchoolCategory = {
            Category: {
                items: ["Kindergarten", "Primary", "Secondary"],
                searchEnabled: true,
                placeholder: "  [Select One]",
                visible: true,
                title: "Class Level",
                showPopupTitle: true
            }
        }

        $scope.SchoolGender = {
            Gender: {
                items: ["Male", "Female", "Mixed"],
                searchEnabled: true,
                placeholder: "  [Select One]",
                visible: true,
                title: "Gender",
                showPopupTitle: true
            }
        }

        $scope.update = function (data) {
            data.LogoUrl = myUrl + "images/schoolLogos/" + data.LogoUrl[0].name;
            $scope.updateData = {
                "AddressLine": data.Address,
                "Name": data.Name,
                "AdministratorId": $sessionStorage.UserId,
                "County": {
                    "Code": 0,
                    "Name": data.County.Name
                },
                "Email": data.Email,
                "ID": $scope.ID,
                "Location": {
                    "Latitude": 2.0,
                    "Longitude": 2.1,
                    "Name": data.Location.Name
                },
                "LogoUrl": data.LogoUrl,
                "SchoolType": {
                    "Boarding": data.SchoolType.Boarding,
                    "Gender": data.SchoolType.Gender,
                    "Category": data.SchoolType.Category
                },
                "Motto": data.Motto,
                "Vission": data.Vission,
                "Mission": data.Mission,
                "Bio": data.Bio,
                "PostalCode": data.PostalCode,
                "Phone": data.Phone,
                "Timestamp": new Date()
            };
            $http.put(myUrl + "api/Schools/" + $scope.ID, $scope.updateData).then(function (response) {
                if (response.status) {
                    toaster.pop('success', "Profile updated");
                }
            }, function (response) {
                toaster.pop('error', "could not update profile");
            });

        }





    }]);