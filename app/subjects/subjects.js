/**
 * Created by clintonb on 4/23/2017.
 */
'use strict';

angular.module('ElimuShare.subjects', ['ngRoute', 'ngStorage'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/subjects', {
            templateUrl: 'subjects/subjects.html',
            controller: 'SubjectsCtrl'
        });
    }])


    .controller('SubjectsCtrl', ['$scope', 'toaster', '$http', '$location', '$sessionStorage', 'paginationService', 'myUrl', '$filter', function ($scope, toaster, $http, $location, $sessionStorage, paginationService, myUrl, $filter) {
        if ($sessionStorage.UserId != null && $sessionStorage.SchoolId != null) {
            $http.get(myUrl + "api/SchoolByAdministratorId?AdministratorId=" + $sessionStorage.UserId).then(function (response) {
                var results = response.data;

                $scope.LogoUrl = results.LogoUrl;
                $scope.Name = results.Name;
                $sessionStorage.SchoolId = results.ID;
                $sessionStorage.LogoUrl = $scope.LogoUrl;
                $sessionStorage.Name = $scope.Name;
                $sessionStorage.SchoolType = results.SchoolType;

                $scope.loadPageFunctions();


            }, function (response) {
                toaster.pop('error', 'Error', "Could not load data, check your internet connectivity");
            });
        } else {
            $location.path('/login');
        }

        $scope.loadPageFunctions = function () {
            $http.get(myUrl + "api/SubjectsBySchool?Id=" + $sessionStorage.SchoolId)
                .then(function (response) {
                    var results = response.data;
                    $scope.subjects = results;
                    $scope.addSubjectLU = {
                        subjectLookup: {
                            dataSource: $scope.subjects,
                            valueExpr: "ID",
                            displayExpr: "Name",
                            searchEnabled: true,
                            placeholder: "  [Select One]",
                            visible: true,
                            title: "Subjects",
                            itemTemplate: function (data, index, container) {
                                var row = $("<div>").addClass("row-fluid");
                                $("<div>").addClass("col-xs-5").text(data["Name"]).appendTo(row);
                                $("<div>").addClass("col-xs-5").text(data["Description"]).appendTo(row);
                                container.append(row);

                            },
                            showPopupTitle: true
                        }
                    }


                }, function (response) {
                    if (response.status == 404) {
                        toaster.pop('info', 'Info', "Please add a subject");
                    } else if (response.status == 500) {
                        toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                    } else {
                        toaster.pop('error', 'Error', "Could not load subjects, please try again, if the issue persists contact system admin");
                    }
                });

            $http.get(myUrl + "api/DxSubjectScoreBySchoolId?SchoolId=" + $sessionStorage.SchoolId)
                .then(function (response) {
                    var results = response.data;
                    $scope.subjectScores = results;
                    $scope.subjectScoresAll = results;
                }, function (response) {
                    if (response.status == 404) {
                        toaster.pop('info', 'Info', "Please add subject results");
                    } else if (response.status == 500) {
                        toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                    } else {
                        toaster.pop('error', 'Error', "Could not load subject results, please try again, if the issue persists contact system admin");
                    }
                });

            $http.get(myUrl + "api/ExamsBySchool?Id=" + $sessionStorage.SchoolId)
                .then(function (response) {
                    var results = response.data;
                    $scope.exams = results;
                    $scope.addExamLU = {
                        examLookup: {
                            dataSource: $scope.exams,
                            valueExpr: "ID",
                            displayExpr: "Name",
                            searchEnabled: true,
                            placeholder: "  [Select One]",
                            visible: true,
                            title: "Exams",
                            itemTemplate: function (data, index, container) {
                                var row = $("<div>").addClass("row-fluid");
                                $("<div>").addClass("col-xs-5").text(data["Name"]).appendTo(row);
                                $("<div>").addClass("col-xs-5").text(data["Description"]).appendTo(row);
                                container.append(row);

                            },
                            showPopupTitle: true
                        }
                    }


                }, function (response) {
                    if (response.status == 404) {
                        toaster.pop('info', 'Info', "Please add an exam");
                    } else if (response.status == 500) {
                        toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                    } else {
                        toaster.pop('error', 'Error', "Could not load exams, please try again, if the issue persists contact system admin");
                    }
                });

            $http.get(myUrl + "api/StudentsBySchoolId?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {
                var results = response.data;
                $scope.studentData = results;
                $scope.addStudentLU = {
                    studentLookup: {
                        dataSource: $scope.studentData,
                        valueExpr: "ID",
                        displayExpr: "SurName",
                        searchEnabled: true,
                        placeholder: "  [Select One]",
                        visible: true,
                        title: "Students",
                        itemTemplate: function (data, index, container) {
                            var row = $("<div>").addClass("row-fluid");
                            $("<div>").addClass("col-xs-3").text(data["FirstName"]).appendTo(row);
                            $("<div>").addClass("col-xs-3").text(data["SecondName"]).appendTo(row);
                            $("<div>").addClass("col-xs-3").text(data["SurName"]).appendTo(row);
                            container.append(row);

                        },
                        showPopupTitle: true
                    }
                }

            }, function (response) {
                if (response.status == 404) {
                    toaster.pop('info', 'Info', "Please adda student");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Could not load students, please try again, if the issue persists contact system admin");
                }

            });

        }

        $scope.clearData = function () {
            $scope.data = [];
        }

        $scope.filterData = function (subject) {
            $scope.subjectScores = $filter('filter')($scope.subjectScoresAll, { Subject: subject.Name });
        }


        $scope.parentForm = {
            Name: {
                placeholder: "Name"
            },
            Description: {
                placeholder: "Description"
            }
        }

        $scope.Term = {
            Type: {
                items: [1, 2, 3],
                searchEnabled: true,
                placeholder: "  [Select One]",
                visible: true,
                title: "Term",
                showPopupTitle: true
            }
        }

        //subject validatioin rules
        var validationGroup = "subject";
        var validationGroupSubjectScore = "subjectScore";

        $("#summarySubject").dxValidationSummary({
            validationGroup: validationGroup
        });

        $scope.subjectNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "Name is required"
            }],
            validationGroup: validationGroup
        };

        $scope.subjectDescriptionValidationRules = {
            validationRules: [{
                type: "required",
                message: "Description is required"
            }],
            validationGroup: validationGroup
        };

        $scope.submitButtonOptionsAddSubject = {
            text: 'Save',
            type: 'success',
            useSubmitBehavior: true,
            validationGroup: validationGroup
        };

        //subjectScore validation rules

        $scope.studentValidationRules = {
            validationRules: [{
                type: "required",
                message: "Student is required"
            }]
        };

        $scope.subjectValidationRules = {
            validationRules: [{
                type: "required",
                message: "Subject is required"
            }]
        };

        $scope.examValidationRules = {
            validationRules: [{
                type: "required",
                message: "Exam is required"
            }]
        };

        $scope.titleValidationRules = {
            validationRules: [{
                type: "required",
                message: "Title is required"
            }]
        };

        $scope.termValidationRules = {
            validationRules: [{
                type: "required",
                message: "Term is required"
            }]
        };

        $scope.scoreValidationRules = {
            validationRules: [{
                type: "required",
                message: "Score is required"
            }]
        };

        $scope.totalScoreValidationRules = {
            validationRules: [{
                type: "required",
                message: "Total score is required"
            }]
        };

        $scope.gradeValidationRules = {
            validationRules: [{
                type: "required",
                message: "Grade is required"
            }]
        };

        $scope.teacherRemarksValidationRules = {
            validationRules: [{
                type: "required",
                message: "Teacher Remarks is required"
            }]
        };


        $scope.submitButtonOptionsAddSubjectScore = {
            text: 'Save',
            type: 'success',
            useSubmitBehavior: true
        };

        var t = 1;
        $scope.onFormSubmitSubjectAdd = function (e) {
            $scope.data.SchoolId = $sessionStorage.SchoolId;
            t++;
            if (t === 2) {
                $scope.subjectData = {
                    "SchoolId": $sessionStorage.SchoolId,
                    "Name": $scope.data.Name,
                    "Description": $scope.data.Description
                }
                $http.post(myUrl + "api/Subjects", $scope.subjectData).then(function (response) {
                    $scope.subjects = [];
                    $scope.loadPageFunctions();
                    $('#con-close-modal-addNewSubject').modal('hide');
                    t = 1;
                    toaster.pop('success', 'Success', "Record Added");
                }, function (response) {
                    t = 1;
                    if (response.status == 409) {
                        toaster.pop('error', 'Error', "Record Already Exists");
                    } else if (response.status == 500) {
                        toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                    } else {
                        toaster.pop('error', 'Error', "Could not add new record please try again. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
                    }

                });
            }


            e.preventDefault();
        }



        $scope.onFormSubmitAddNewSubjectScore = function (e) {
            $scope.data.SchoolId = $sessionStorage.SchoolId;
            $scope.data.Year = new Date();
            $scope.subjectScoreData = {
                "SubjectId": $scope.data.SubjectId,
                "ExamId": $scope.data.ExamId,
                "SchoolId": $sessionStorage.SchoolId,
                "StudentId": $scope.data.StudentId,
                "Title": $scope.data.Title,
                "Year": $scope.data.Year,
                "Term": $scope.data.Term,
                "Score": $scope.data.Score,
                "TotalScore": $scope.data.TotalScore,
                "Grade": $scope.data.Grade,
                "TeacherRemarks": $scope.data.TeacherRemarks
            }
            $http.post(myUrl + "api/SubjectScores", $scope.subjectScoreData).then(function (response) {
                $scope.subjectScores = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-addNewSubjectScore').modal('hide');
                toaster.pop('success', 'Success', "Record Added");
            }, function (response) {
                if (response.status == 409) {
                    toaster.pop('error', 'Error', "Record Already Exists");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Could not add record please try again. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
                }

            });
            e.preventDefault();
        }

        $scope.delete = function (data) {
            $scope.SubjectScoreDeleteId = data.ID;
        }

        $scope.saveDelete = function () {
            $http.delete(myUrl + "api/SubjectScores/" + $scope.SubjectScoreDeleteId).then(function (response) {
                var results = response.data;
                $scope.subjectScores = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-deleteSubjectScore').modal('hide');
                toaster.pop('success', 'Success', "Record Deleted");
            }, function (response) {
                if (response.status == 404) {
                    toaster.pop('info', 'Error', "No data found");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Could not delete record, please try again. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
                }

            });
        }

        $scope.deleteSubject = function (data) {
            $scope.SubjectDeleteId = data.ID;
        }

        $scope.saveDeleteSubject = function () {
            $http.delete(myUrl + "api/Subjects/" + $scope.SubjectDeleteId).then(function (response) {
                $scope.subjects = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-deleteSubject').modal('hide');
                toaster.pop('success', 'Success', "Record Deleted");
            }, function (response) {
                if (response.status == 404) {
                    toaster.pop('info', 'Error', "No data found");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Could not delete record, please try again. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
                }

            });
        }

        $scope.editSubject = function (data) {
            $scope.data = [];
            $scope.subjecteditid = data.ID;
            $scope.data.Name = data.Name;
            $scope.data.Description = data.Description;
        }

        $scope.saveEditSubjectChanges = function () {
            $scope.data.SchoolId = $sessionStorage.SchoolId
            $scope.newData = {
                "SchoolId": $sessionStorage.SchoolId,
                "Name": $scope.data.Name,
                "Description": $scope.data.Description
            }
            $http.put(myUrl + "api/Subjects/" + $scope.subjecteditid, $scope.newData).then(function (response) {
                $scope.subjects = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-editSubject').modal('hide');
                toaster.pop('success', 'Success', "Record Edited");
            }, function (response) {
                if (response.status == 404) {
                    toaster.pop('info', 'No data', "No data found");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Could not edit record, please try again. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
                }

            });

        }

        $scope.edit = function (data) {
            $scope.data = [];
            $scope.subjectScoreeditid = data.ID;
            $scope.data.Title = data.Title;
            $scope.data.Term = data.Term;
            $scope.data.Score = data.Score;
            $scope.data.TotalScore = data.TotalScore;
            $scope.data.Grade = data.Grade;
            $scope.data.TeacherRemarks = data.TeacherRemarks;
        }

        $scope.saveEdit = function (data) {

            $scope.data.SchoolType = $sessionStorage.SchoolType;
            $scope.data.SchoolId = $sessionStorage.SchoolId
            $scope.data.Year = new Date();
            $scope.newData = {
                "StudentId": $scope.data.StudentId,
                "ExamId": $scope.data.ExamId,
                "Grade":$scope.data.Grade,
                "SchoolId":$scope.data.SchoolId,
                "SchoolType":$scope.data.SchoolType,
                "Score":$scope.data.Score,
                "SubjectId":$scope.data.SubjectId,
                "TeacherRemarks":$scope.data.TeacherRemarks,
                "Term":$scope.data.Term,
                "Title":$scope.data.Title,
                "TotalScore":$scope.data.TotalScore,
                "Year":$scope.data.Year
            }

            $http.put(myUrl + "api/SubjectScores/" + $scope.subjectScoreeditid, $scope.newData).then(function (response) {
                $scope.subjectScores = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-editSubjectScore').modal('hide');
                toaster.pop('success', 'Success', "Record Edited");
            }, function (response) {
                if (response.status == 404) {
                    toaster.pop('info', 'Error', "No data found");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Could not edit record, please try again. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
                }

            });


        }


        $scope.subjectScoreForm = {
            Title: {
                placeholder: "Title"
            },
            Term: {
                placeholder: "Term"
            },
            Score: {
                placeholder: "Score"
            },
            TotalScore: {
                placeholder: "TotalScore"
            },
            Grade: {
                placeholder: "Grade"
            },
            TeacherRemarks: {
                placeholder: "TeacherRemarks"
            }
        }

        //get subject scores


    }]);