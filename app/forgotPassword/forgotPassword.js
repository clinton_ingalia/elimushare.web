/**
 * Created by clintonb on 4/13/2017.
 */
'use strict';

angular.module('ElimuShare.forgotPassword', ['ngRoute', 'toaster', 'ngAnimate', 'ngStorage'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/forgotPassword', {
            templateUrl: 'forgotPassword/forgotPassword.html',
            controller: 'ForgotPasswordCtrl'
        });
    }])

    .controller('ForgotPasswordCtrl', ['$scope', 'toaster', '$http', '$location', '$sessionStorage', 'myUrl', function ($scope, toaster, $http, $location, $sessionStorage, myUrl) {
        $scope.user = {
            MobileNumber: ""
        };

        $scope.mobileNumberValidationRules = {
            validationRules: [{
                type: "required",
                message: "Mobile number is required"
            }]
        };


        $scope.onFormSubmit = function (e) {
            $scope.user.MobileNumberDirty = $scope.user.MobileNumberDirtyB;
            $scope.forgotPasswordInput.mobileNumber.value = $scope.user.MobileNumberDirtyB;
            if($scope.user.MobileNumberDirty.charAt(0) === "0"){
                $scope.user.MobileNumberDirty = "+254" + $scope.user.MobileNumberDirty.substr(1);
            }else if($scope.user.MobileNumberDirty.charAt(0) === "7"){
                $scope.user.MobileNumberDirty = "+254" + $scope.user.MobileNumberDirty;
            }else if ($scope.user.MobileNumberDirty.charAt(0) === "2") {
                $scope.user.MobileNumberDirty = "+" + $scope.user.MobileNumberDirty;
            }else if ($scope.user.MobileNumberDirty.charAt(0) === "+") {
                //Do nothing.
            }
            $scope.user.MobileNumber = $scope.user.MobileNumberDirty;
            $http.post(myUrl + "api/UserForgotPassword", $scope.user).then(function (response) {
                $sessionStorage.UserId = response.data.ID;
                toaster.pop('success', "Reset Password Successfull", "Kindly Login");
                $location.path('/login');
            }, function (response) {
                toaster.pop('error', response.data, "Check mobile number");
            });
            e.preventDefault();
        };

        $scope.submitButtonOptions = {
            text: 'Reset Password',
            type: 'success',
            useSubmitBehavior: true
        };

        $scope.forgotPasswordInput = {
            mobileNumber: {
                mode:"tel",
                placeholder:"Enter mobile number 0712 345 678",
                showClearButton: true
            }
        }
    }]);