'use strict';

// Declare app level module which depends on views, and components
angular.module('ElimuShare', [
    'ngRoute',
    'toaster',
    'ngAnimate',
    'ngStorage',
    'angular-loading-bar',
    'dx',
    'ui.bootstrap',
    'angularUtils.directives.dirPagination',
    'mgo-angular-wizard',
    'ElimuShare.dashboard',
    'ElimuShare.students',
    'ElimuShare.parents',
    'ElimuShare.subjects',
    'ElimuShare.exams',
    'ElimuShare.fees',
    'ElimuShare.events',
    'ElimuShare.notifications',
    'ElimuShare.uploader',
    'ElimuShare.view2',
    'ElimuShare.login',
    'ElimuShare.logout',
    'ElimuShare.forgotPassword',
    'ElimuShare.register',
    'ElimuShare.profile'
]).constant('myUrl', 'http://elimushareapilive.azurewebsites.net/').config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.otherwise({redirectTo: '/login'});
}]).config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push('authInterceptor');
}]).factory('authInterceptor', [
    "$q", "$window", "$location", "$sessionStorage", function($q, $window, $location, $sessionStorage) {
        return {
            request: function(config) {
                config.headers = config.headers || {};
                config.headers.Authorization = 'Bearer ' + $sessionStorage.AccessToken; // add your token from your service or whatever
                return config;
            },
            response: function(response) {
                return response || $q.when(response);
            },
            responseError: function(rejection) {
                // your error handler
            }
        };
    }
]);
