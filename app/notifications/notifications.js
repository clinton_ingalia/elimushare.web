/**
 * Created by clintonb on 4/23/2017.
 */
'use strict';

angular.module('ElimuShare.notifications', ['ngRoute', 'ngStorage'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/notifications', {
            templateUrl: 'notifications/notifications.html',
            controller: 'NotificationsCtrl'
        });
    }])

    .controller('NotificationsCtrl', ['$scope', 'toaster', '$http', '$location', '$sessionStorage', 'myUrl', function ($scope, toaster, $http, $location, $sessionStorage, myUrl) {
        if ($sessionStorage.UserId != null && $sessionStorage.SchoolId != null) {
            $http.get(myUrl + "api/SchoolByAdministratorId?AdministratorId=" + $sessionStorage.UserId).then(function (response) {
                var results = response.data;

                $scope.LogoUrl = results.LogoUrl;
                $scope.Name = results.Name;
                $sessionStorage.SchoolId = results.ID;
                $sessionStorage.LogoUrl = $scope.LogoUrl;
                $sessionStorage.Name = $scope.Name;

                $scope.loadPageFunctions();

            }, function (response) {
                toaster.pop('error', 'Error', "Could not load data, check your internet connectivity");
            });
        } else {
            $location.path('/login');
        }

        $scope.loadPageFunctions = function () {
            $http.get(myUrl + "api/NotificationsBySchoolId?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {
                var results = response.data;
                $scope.notifications = results;

            }, function (response) {
                toaster.pop('error', 'Error', "Could not load data, kindly refresh the page.");
            });
        }

        $scope.delete = function (data) {
            $scope.notificationDeleteId = data.ID;
        }

        $scope.saveDelete = function () {
            $http.delete(myUrl + "api/Notifications/" + $scope.notificationDeleteId).then(function (response) {
                $scope.notifications = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-deleteNotification').modal('hide');
                toaster.pop('success', 'Record Deleted', "Record successfully deleted");
            }, function (response) {

                toaster.pop('error', 'Error', "Could not delete record. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
            });
        }

        $scope.titleValidationRules = {
            validationRules: [{
                type: "required",
                message: "Title is required"
            }]
        };

        $scope.descriptionValidationRules = {
            validationRules: [{
                type: "required",
                message: "Description is required"
            }]
        };

        $scope.publicValidationRules = {
            validationRules: [{
                type: "required",
                message: "Public is required"
            }]
        };

        $scope.submitButtonOptions = {
            text: 'Save',
            type: 'success',
            useSubmitBehavior: true
        };


        $scope.onFormSubmitNotification = function (e) {
            $scope.data.SchoolId = $sessionStorage.SchoolId;
            $scope.data.SchoolName = $sessionStorage.Name;
            $scope.notificationData = {
                "SchoolId": $scope.data.SchoolId,
                "SchoolName": $scope.data.SchoolName,
                "Title": $scope.data.Title,
                "Description": $scope.data.Description,
                "Public": $scope.data.Public
            }
            $http.post(myUrl + "api/Notifications", $scope.notificationData).then(function (response) {
                $scope.notifications = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-addNewNotification').modal('hide');
                toaster.pop('success', 'Record Added', "Record successfully added");
            }, function (response) {
                toaster.pop('error', 'Error', "Could not add record. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
            });
            e.preventDefault();
        }

        $scope.edit = function (data) {
            $scope.data = [];
            console.log($sessionStorage);
            $scope.notificationeditid = data.ID;
            $scope.data.SchoolId = $sessionStorage.SchoolId;
            $scope.data.SchoolName = $sessionStorage.Name;
            $scope.data.Title = data.Title;
            $scope.data.Description = data.Description;
            $scope.data.Public = data.Public;
        }

        $scope.clearData = function () {
            $scope.data = [];
        }

        $scope.Public = {
            Type: {
                items: ["True", "False"],
                searchEnabled: true,
                placeholder: "  [Select One]",
                visible: true,
                title: "Public",
                showPopupTitle: true
            }
        }

        $scope.saveEdit = function () {
            $scope.notificationEditData = {
                "SchoolId": $scope.data.SchoolId,
                "SchoolName": $scope.data.SchoolName,
                "Title": $scope.data.Title,
                "Description": $scope.data.Description,
                "Public": $scope.data.Public
            }
            $http.put(myUrl + "api/Notifications/" + $scope.notificationeditid, $scope.notificationEditData).then(function (response) {
                $scope.data = [];
                $scope.notifications = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-editNotification').modal('hide');
                toaster.pop('success', 'Record Edited', "Record successfully edited");
            }, function (response) {

                toaster.pop('error', 'Error', "Could not edit record. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
            });

        }






    }]);