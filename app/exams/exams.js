/**
 * Created by clintonb on 4/23/2017.
 */
'use strict';

angular.module('ElimuShare.exams', ['ngRoute', 'ngStorage'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/exams', {
            templateUrl: 'exams/exams.html',
            controller: 'ExamsCtrl'
        });
    }])

    .controller('ExamsCtrl', ['$scope', 'toaster', '$http', '$location', '$sessionStorage', 'myUrl', '$filter', function ($scope, toaster, $http, $location, $sessionStorage, myUrl, $filter) {
        if ($sessionStorage.UserId != null && $sessionStorage.SchoolId != null) {
            $http.get(myUrl + "api/SchoolByAdministratorId?AdministratorId=" + $sessionStorage.UserId).then(function (response) {
                var results = response.data;
                $scope.LogoUrl = results.LogoUrl;
                $scope.Name = results.Name;
                $sessionStorage.SchoolId = results.ID;
                $sessionStorage.LogoUrl = $scope.LogoUrl;
                $sessionStorage.Name = $scope.Name;
                $sessionStorage.SchoolType = results.SchoolType;

                $scope.loadPageFunctions();

            }, function (response) {
                toaster.pop('error', 'Error', "Could not load data, check your internet connectivity");
            });
        } else {
            $location.path('/login');
        }

        $scope.loadPageFunctions = function () {
            $http.get(myUrl + "api/ExamsBySchool?Id=" + $sessionStorage.SchoolId)
                .then(function (response) {
                    var results = response.data;
                    $scope.exams = results;
                    $scope.addExamLU = {
                        examLookup: {
                            dataSource: $scope.exams,
                            valueExpr: "ID",
                            displayExpr: "Name",
                            searchEnabled: true,
                            placeholder: "  [Select One]",
                            visible: true,
                            title: "Exams",
                            itemTemplate: function (data, index, container) {
                                var row = $("<div>").addClass("row-fluid");
                                $("<div>").addClass("col-xs-5").text(data["Name"]).appendTo(row);
                                $("<div>").addClass("col-xs-5").text(data["Description"]).appendTo(row);
                                container.append(row);

                            },
                            showPopupTitle: true
                        }
                    }

                }, function (response) {
                    if (response.status == 404) {
                        toaster.pop('info', 'No data', "Please add an exam");
                    } else if (response.status == 500) {
                        toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                    } else {
                        toaster.pop('error', 'Error', "Could not load exams, please try again, if the issue persists contact system admin");
                    }
                });


            $http.get(myUrl + "api/DxSubjectScoreBySchoolId?SchoolId=" + $sessionStorage.SchoolId)
                .then(function (response) {
                    var results = response.data;
                    $scope.subjectScores = results;
                    $scope.subjectScoresAll = results;
                }, function (response) {
                    if (response.status == 404) {
                        toaster.pop('info', 'Info', "Please add subject results");
                    } else if (response.status == 500) {
                        toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                    } else {
                        toaster.pop('error', 'Error', "Could not load subject results, please try again, if the issue persists contact system admin");
                    }
                });


            $http.get(myUrl + "api/StudentsBySchoolId?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {
                var results = response.data;
                $scope.studentData = results;
                $scope.addStudentLU = {
                    studentLookup: {
                        dataSource: $scope.studentData,
                        valueExpr: "ID",
                        displayExpr: "SurName",
                        searchEnabled: true,
                        placeholder: "  [Select One]",
                        visible: true,
                        title: "Students",
                        itemTemplate: function (data, index, container) {
                            var row = $("<div>").addClass("row-fluid");
                            $("<div>").addClass("col-xs-3").text(data["FirstName"]).appendTo(row);
                            $("<div>").addClass("col-xs-3").text(data["SecondName"]).appendTo(row);
                            $("<div>").addClass("col-xs-3").text(data["SurName"]).appendTo(row);
                            container.append(row);

                        },
                        showPopupTitle: true
                    }
                }
            }, function (response) {
                if (response.status == 404) {
                    toaster.pop('info', 'No data', "Please add a student");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Could not load students, please try again, if the issue persists contact system admin");
                }

            });

            $http.get(myUrl + "api/DxExamScoreBySchoolId?SchoolId=" + $sessionStorage.SchoolId)
                .then(function (response) {
                    var results = response.data;
                    $scope.examScores = results;
                    $scope.examScoresAll = results;

                }, function (response) {
                    if (response.status == 404) {
                        toaster.pop('info', 'No data', "Please add exam results");
                    } else if (response.status == 500) {
                        toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                    } else {
                        toaster.pop('error', 'Error', "Could not load exam results, please try again, if the issue persists contact system admin");
                    }
                });


        }


        $scope.examScoreForm = {
            Title: {
                placeholder: "Exam Title"
            },
            Term: {
                placeholder: "Term"
            },
            Position: {
                placeholder: "Position"
            },
            TotalStudents: {
                placeholder: "TotalStudents"
            },
            Score: {
                placeholder: "Score"
            },
            TotalScore: {
                placeholder: "TotalScore"
            },
            Grade: {
                placeholder: "Grade"
            },
            TeacherRemarks: {
                placeholder: "TeacherRemarks"
            },
        }

        $scope.parentForm = {
            Name: {
                placeholder: "Name"
            },
            Description: {
                placeholder: "Description"
            },Term: {
                placeholder: "Term"
            },Year: {
                placeholder: "Year"
            }
        }

        $scope.Term = {
            Type: {
                items: [1, 2, 3],
                searchEnabled: true,
                placeholder: "  [Select One]",
                visible: true,
                title: "Class Level",
                showPopupTitle: true
            }
        }

        $scope.clearData = function () {
            $scope.data = [];
        }

        $scope.filterData = function (exam) {
            $scope.examScores = $filter('filter')($scope.examScoresAll, { Exam: exam.Name });
        }

        $scope.filterViewData = function (examScore) {
            $scope.subjectScores = $filter('filter')($scope.subjectScoresAll, { Exam: examScore.Exam });
        }

        //subject validatioin rules
        var validationGroup = "exam";
        var validationGroupExamScore = "examScore";

        $("#summaryExam").dxValidationSummary({
            validationGroup: validationGroup
        });

        $scope.examNameValidationRules = {
            validationRules: [{
                type: "required",
                message: "Name is required"
            }],
            validationGroup: validationGroup
        };

        $scope.examDescriptionValidationRules = {
            validationRules: [{
                type: "required",
                message: "Description is required"
            }],
            validationGroup: validationGroup
        };

        $scope.examTermValidationRules = {
            validationRules: [{
                type: "required",
                message: "Term is required"
            }],
            validationGroup: validationGroup
        };

        $scope.examYearValidationRules = {
            validationRules: [{
                type: "required",
                message: "Year is required"
            }],
            validationGroup: validationGroup
        };

        $scope.submitButtonOptionsAddExam = {
            text: 'Save',
            type: 'success',
            useSubmitBehavior: true,
            validationGroup: validationGroup
        };

        //subjectScore validation rules

        $scope.studentValidationRules = {
            validationRules: [{
                type: "required",
                message: "Student is required"
            }]
        };


        $scope.examValidationRules = {
            validationRules: [{
                type: "required",
                message: "Exam is required"
            }]
        };

        $scope.titleValidationRules = {
            validationRules: [{
                type: "required",
                message: "Title is required"
            }]
        };

        $scope.termValidationRules = {
            validationRules: [{
                type: "required",
                message: "Term is required"
            }]
        };

        $scope.scoreValidationRules = {
            validationRules: [{
                type: "required",
                message: "Score is required"
            }]
        };

        $scope.totalScoreValidationRules = {
            validationRules: [{
                type: "required",
                message: "Total score is required"
            }]
        };

        $scope.positionValidationRules = {
            validationRules: [{
                type: "required",
                message: "Score is required"
            }]
        };

        $scope.totalStudentsValidationRules = {
            validationRules: [{
                type: "required",
                message: "Total score is required"
            }]
        };

        $scope.gradeValidationRules = {
            validationRules: [{
                type: "required",
                message: "Grade is required"
            }]
        };

        $scope.teacherRemarksValidationRules = {
            validationRules: [{
                type: "required",
                message: "Teacher Remarks is required"
            }]
        };


        $scope.submitButtonOptionsAddExamScore = {
            text: 'Save',
            type: 'success',
            useSubmitBehavior: true
        };

        var t = 1;
        $scope.onFormSubmitExamAdd = function (e) {
            $scope.data.SchoolId = $sessionStorage.SchoolId;
            t++;
            if (t === 2) {
                $scope.examData = {
                    "SchoolId": $sessionStorage.SchoolId,
                    "Name": $scope.data.Name,
                    "Description": $scope.data.Description,
                    "Term": $scope.data.Term,
                    "Year": $scope.data.Year
                }
                $http.post(myUrl + "api/Exams", $scope.examData).then(function (response) {
                    $scope.exams = [];
                    $scope.loadPageFunctions();
                    $('#con-close-modal-addNewExam').modal('hide');
                    t = 1;
                    toaster.pop('success', 'Success', "Record Added");
                }, function (response) {
                    t = 1;
                    if (response.status == 409) {
                        toaster.pop('error', 'Error', "Record Already Exists");
                    } else if (response.status == 500) {
                        toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                    } else {
                        toaster.pop('error', 'Error', "Could not add new record please try again. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
                    }

                });
            }
            e.preventDefault();
        }

        $scope.onFormSubmitAddNewExamScore = function (e) {
            $scope.data.SchoolId = $sessionStorage.SchoolId;
            $scope.data.Year = new Date();
            $scope.examScoreData = {
                "StudentId": $scope.data.StudentId,
                "ExamId": $scope.data.ExamId,
                "SchoolId": $sessionStorage.SchoolId,
                "Title": $scope.data.Title,
                "Year": $scope.data.Year,
                "Term": $scope.data.Term,
                "Position": $scope.data.Position,
                "TotalStudents": $scope.data.TotalStudents,
                "Score": $scope.data.Score,
                "TotalScore": $scope.data.TotalScore,
                "Grade": $scope.data.Grade,
                "TeacherRemarks": $scope.data.TeacherRemarks
            }
            $http.post(myUrl + "api/ExamScores", $scope.examScoreData).then(function (response) {
                $scope.examScores = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-addNewExamScore').modal('hide');
                toaster.pop('success', 'Success', "Record Added");
            }, function (response) {
                if (response.status == 409) {
                    toaster.pop('error', 'Error', "Record Already Exists");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Could not add record please try again. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
                }

            });
            e.preventDefault();
        }

        $scope.delete = function (data) {
            $scope.ExamScoreDeleteId = data.ID;
        }

        $scope.saveDelete = function () {
            $http.delete(myUrl + "api/ExamScores/" + $scope.ExamScoreDeleteId).then(function (response) {
                $scope.examScores = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-deleteExamScore').modal('hide');
                toaster.pop('success', 'Success', "Record Deleted");
            }, function (response) {
                if (response.status == 404) {
                    toaster.pop('info', 'No data', "No data found");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Could not delete record, please try again. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
                }

            });
        }

        $scope.deleteExam = function (data) {
            $scope.ExamDeleteId = data.ID;
        }

        $scope.saveDeleteExam = function () {
            $http.delete(myUrl + "api/Exams/" + $scope.ExamDeleteId).then(function (response) {
                $scope.exams = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-deleteExam').modal('hide');
                toaster.pop('success', 'Success', "Record Deleted");
            }, function (response) {
                if (response.status == 404) {
                    toaster.pop('info', 'Error', "No data found");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Could not delete record, please try again. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
                }

            });
        }

        $scope.editExam = function (data) {
            $scope.data = [];
            $scope.exameditid = data.ID;
            $scope.data.Name = data.Name;
            $scope.data.Description = data.Description;
        }

        $scope.saveEditExamChanges = function () {
            $scope.data.SchoolId = $sessionStorage.SchoolId
            $scope.newData = {
                "SchoolId": $sessionStorage.SchoolId,
                "Name": $scope.data.Name,
                "Description": $scope.data.Description
            }
            $http.put(myUrl + "api/Exams/" + $scope.exameditid, $scope.newData).then(function (response) {
                $scope.exams = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-editExam').modal('hide');
                toaster.pop('success', 'Success', "Record Edited");
            }, function (response) {
                if (response.status == 404) {
                    toaster.pop('info', 'No data', "No data found");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Could not edit record, please try again. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
                }

            });

        }

        $scope.edit = function (data) {
            $scope.data = [];
            $scope.examScoreeditid = data.ID;
            $scope.data.Title = data.Title;
            $scope.data.Term = data.Term;
            $scope.data.Score = data.Score;
            $scope.data.TotalScore = data.TotalScore;
            $scope.data.Position = data.Position;
            $scope.data.TotalStudents = data.TotalStudents;
            $scope.data.Grade = data.Grade;
            $scope.data.TeacherRemarks = data.TeacherRemarks;
            $scope.addStudentLU.studentLookup.value = data.StudentId;
            $scope.data.ExamId = data.ExamId;
        }

        $scope.saveEdit = function (data) {
            $scope.data.SchoolType = $sessionStorage.SchoolType;
            $scope.data.SchoolId = $sessionStorage.SchoolId
            $scope.data.Year = new Date();
            $scope.newData = {
                "StudentId": $scope.data.StudentId,
                "ExamId": $scope.data.ExamId,
                "Grade":$scope.data.Grade,
                "SchoolId":$scope.data.SchoolId,
                "SchoolType":$scope.data.SchoolType,
                "Score":$scope.data.Score,
                "Position":$scope.data.Position,
                "TeacherRemarks":$scope.data.TeacherRemarks,
                "Term":$scope.data.Term,
                "Title":$scope.data.Title,
                "TotalScore":$scope.data.TotalScore,
                "TotalStudents":$scope.data.TotalStudents,
                "Year":$scope.data.Year
            }
            $http.put(myUrl + "api/ExamScores/" + $scope.examScoreeditid, $scope.newData).then(function (response) {
                var results = response.data;
                $scope.examScores = [];
                $scope.loadPageFunctions();
                $('#con-close-modal-editExamScore').modal('hide');
                toaster.pop('success', 'Success', "Record Edited");
            }, function (response) {
                if (response.status == 404) {
                    toaster.pop('info', 'Error', "No data found");
                } else if (response.status == 500) {
                    toaster.pop('error', 'Info', "Server is experiencing issues, please try again, if the issue persists contact system admin");
                } else {
                    toaster.pop('error', 'Error', "Could not edit record, please try again. Make sure you have filled and/or selected all required records, if the issue persists contact system admin");
                }

            });

        }

        //get exam scores


        $scope.buttonOptions = {
            text: "Clear",
            onClick: function () {
                clearEvents();
            }
        };


    }]);