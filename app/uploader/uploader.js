/**
 * Created by clintonb on 4/23/2017.
 */
'use strict';

angular.module('ElimuShare.uploader', ['ngRoute', 'ngStorage'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/uploader', {
            templateUrl: 'uploader/uploader.html',
            controller: 'UploaderCtrl'
        });
    }])

    .controller('UploaderCtrl', ['$scope', 'toaster', '$http', '$location', '$sessionStorage', 'myUrl', function ($scope, toaster, $http, $location, $sessionStorage, myUrl) {
        if ($sessionStorage.UserId != null && $sessionStorage.SchoolId != null) {
            $http.get(myUrl + "api/SchoolByAdministratorId?AdministratorId=" + $sessionStorage.UserId).then(function (response) {
                var results = response.data;
                $scope.LogoUrl = results.LogoUrl;
                $scope.Name = results.Name;
                $sessionStorage.SchoolId = results.ID;
                $sessionStorage.LogoUrl = $scope.LogoUrl;
                $sessionStorage.Name = $scope.Name;
            }, function (response) {
                //toaster.pop('error', response.data, "Check mobile number or password");
            });
        } else {
            $location.path('/login');
        }

        $scope.selectedFile = null;
        $scope.selection = {
            Selected: ""
        };
        $scope.uploadButton = true;

        $scope.get_header_row = function (sheet) {
            var headers = [];
            var range = XLSX.utils.decode_range(sheet['!ref']);
            var C, R = range.s.r;
            /* start in the first row */
            /* walk every column in the range */
            for (C = range.s.c; C <= range.e.c; ++C) {
                var cell = sheet[XLSX.utils.encode_cell({c: C, r: R})]
                /* find the cell in the first row */

                var hdr = "UNKNOWN " + C; // <-- replace with your desired default
                if (cell && cell.t) hdr = XLSX.utils.format_cell(cell);

                headers.push(hdr);
            }
            return headers;
        }

        $scope.loadFile = function (files) {

            $scope.$apply(function () {
                $scope.uploadButton = false;
                $scope.selectedFile = files[0];

            })

        }

        $scope.handleFile = function () {

            var file = $scope.selectedFile;

            if (file) {

                var reader = new FileReader();

                reader.onload = function (e) {

                    var data = e.target.result;

                    var workbook = XLSX.read(data, {type: 'binary'});

                    var columns = $scope.get_header_row(workbook.Sheets.Sheet1);

                    var firstcolumnData = workbook.Sheets.Sheet1.A2;

                    debugger;

                    var first_sheet_name = workbook.SheetNames[0];

                    var dataObjects = XLSX.utils.sheet_to_json(workbook.Sheets[first_sheet_name]);

                    debugger;

                    //console.log(excelData);

                    if (dataObjects.length > 0) {

                        console.log($scope.selection);
                        console.log(dataObjects);
                        $scope.save(dataObjects);


                    } else {
                        toaster.pop('error', "error", "could not load file, please check the file type. Make sure its Excel file.");
                    }

                }

                reader.onerror = function (ex) {

                }

                reader.readAsBinaryString(file);
            }
        }


        $scope.save = function (data) {
            var url = "";
            if ($scope.selection.Selected == "Students") {
                url = myUrl + "api/MultipleStudents?SchoolId=" + $sessionStorage.SchoolId;
            } else if ($scope.selection.Selected == "Subject Results") {
                url = myUrl + "api/MultipleSubjectScores?SchoolId=" + $sessionStorage.SchoolId;
            } else if ($scope.selection.Selected == "Exam Results") {
                url = myUrl + "api/MultipleExamScores?SchoolId=" + $sessionStorage.SchoolId;
            } else if ($scope.selection.Selected == "Fees") {
                url = myUrl + "api/MultipleFees?SchoolId=" + $sessionStorage.SchoolId;
                //url = "http://localhost:4335/api/MultipleFees?SchoolId=" + $sessionStorage.SchoolId;
            } else if ($scope.selection.Selected == "Events") {
                url = myUrl + "api/MultipleEvents?schoolid=" + $sessionStorage.SchoolId;
            }
            $http.post(url, data).then(function (response) {
                if (response.status) {
                    toaster.pop('success', "Data imported");
                }
            }, function (response) {
                if(response.status === 406){
                    toaster.pop('error', "could not upload, " + response.data);
                }else{
                    toaster.pop('error', "could not upload, Make sure you used the template provided, if the issue persists contact system admin");
                }

            });
        }

        var types = ["Students", "Subject Results", "Exam Results", "Fees", "Events"];
        $scope.currentType = types[0];
        $scope.typesOptions = {
            dataSource: types,
            bindingOptions: {
                value: "currentType"
            }
        };


    }]);