/**
 * Created by clintonb on 4/13/2017.
 */
'use strict';

angular.module('ElimuShare.login', ['ngRoute', 'toaster', 'ngAnimate', 'ngStorage'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/login', {
            templateUrl: 'login/login.html',
            controller: 'LoginCtrl'
        });
    }])

    .factory('$remember', function() {
        function fetchValue(name) {
            var gCookieVal = document.cookie.split("; ");
            for (var i=0; i < gCookieVal.length; i++)
            {
                // a name/value pair (a crumb) is separated by an equal sign
                var gCrumb = gCookieVal[i].split("=");
                if (name === gCrumb[0])
                {
                    var value = '';
                    try {
                        value = angular.fromJson(gCrumb[1]);
                    } catch(e) {
                        value = unescape(gCrumb[1]);
                    }
                    return value;
                }
            }
            // a cookie with the requested name does not exist
            return null;
        }
        return function(name, values) {
            if(arguments.length === 1) return fetchValue(name);
            var cookie = name + '=';
            if(typeof values === 'object') {
                var expires = '';
                cookie += (typeof values.value === 'object') ? angular.toJson(values.value) + ';' : values.value + ';';
                if(values.expires) {
                    var date = new Date();
                    date.setTime( date.getTime() + (values.expires * 24 *60 * 60 * 1000));
                    expires = date.toGMTString();
                }
                cookie += (!values.session) ? 'expires=' + expires + ';' : '';
                cookie += (values.path) ? 'path=' + values.path + ';' : '';
                cookie += (values.secure) ? 'secure;' : '';
            } else {
                cookie += values + ';';
            }
            document.cookie = cookie;
        }
    })

    .controller('LoginCtrl', ['$scope', 'toaster', '$http', '$location', '$sessionStorage', '$remember', 'myUrl', function ($scope, toaster, $http, $location, $sessionStorage, $remember, myUrl) {
        $scope.user = {
            MobileNumber: "",
            Password: "",
        };

        //$httpProvider.interceptors.push('authInterceptor');

        if ($remember('username') && $remember('password') ) {
            $scope.remember = true;
            $scope.username = $remember('username');
            $scope.password = $remember('password');
        }
        $scope.rememberMe = function() {
            if ($scope.remember) {
                $remember('username', $scope.user.MobileNumber);
                $remember('password', $scope.user.Password);
            } else {
                $remember('username', '');
                $remember('password', '');
            }
        };

        $scope.mobileNumberValidationRules = {
            validationRules: [{
                type: "required",
                message: "Mobile number is required"
            }]
        };

        $scope.passwordValidationRules = {
            validationRules: [{
                type: "required",
                message: "Password is required"
            }]
        };

        $scope.onFormSubmit = function (e) {

            //TODO remember  me not working
            if ($sessionStorage.rememberMe === true) {
                toaster.pop('success', "Login Successfull", "Welcome to ElimuShare");
                $location.path('/dashboard');
            }else{
                $scope.user.MobileNumberDirty = $scope.user.MobileNumberDirtyB;
                $scope.loginInput.mobileNumber.value = $scope.user.MobileNumberDirtyB;
                if($scope.user.MobileNumberDirty.charAt(0) === "0"){
                    $scope.user.MobileNumberDirty = "+254" + $scope.user.MobileNumberDirty.substr(1);
                }else if($scope.user.MobileNumberDirty.charAt(0) === "7"){
                    $scope.user.MobileNumberDirty = "+254" + $scope.user.MobileNumberDirty;
                }else if ($scope.user.MobileNumberDirty.charAt(0) === "2") {
                    $scope.user.MobileNumberDirty = "+" + $scope.user.MobileNumberDirty;
                }else if ($scope.user.MobileNumberDirty.charAt(0) === "+") {
                    //Do nothing.
                }
                $scope.user.MobileNumber = $scope.user.MobileNumberDirty;

                if($scope.user.MobileNumber.length === 13){
                    $http.post(myUrl + "api/Login", $scope.user).then(function (response) {
                        $sessionStorage.UserId = response.data.ID;
                        toaster.pop('success', "Login Successfull", "Welcome to ElimuShare");

                        $http({
                            method: 'POST',
                            url: myUrl + "token",
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            transformRequest: function(obj) {
                                var str = [];
                                for(var p in obj)
                                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                                return str.join("&");
                            },
                            data: {grant_type: "password", username: $scope.user.MobileNumber, password: $scope.user.Password}
                        }).then(function (response) {
                            $sessionStorage.AccessToken = response.data["access_token"];
                            $location.path('/dashboard');
                        });

                    }, function (response) {
                        toaster.pop('error', response.data, "Check mobile number or password");
                        $scope.user.MobileNumber = "";
                    });
                }else{
                    toaster.pop('error', "Error", "Incomplete mobile number");
                }

            }


            e.preventDefault();
        };

        $scope.submitButtonOptions = {
            text: 'Login',
            type: 'success',
            useSubmitBehavior: true
        };

        $scope.loginInput = {
            mobileNumber: {
                mode: 'tel',
                placeholder: "Enter mobile number 0712 345 567",
                showClearButton: true
            },
            password: {
                mode: "password",
                placeholder: "Enter password",
                showClearButton: true
            }
        }
    }]);