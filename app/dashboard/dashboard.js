/**
 * Created by clintonb on 4/20/2017.
 */
'use strict';

angular.module('ElimuShare.dashboard', ['ngRoute', 'ngStorage'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/dashboard', {
            templateUrl: 'dashboard/dashboard.html',
            controller: 'DashboardCtrl'
        });
    }])

    .controller('DashboardCtrl', ['$scope', 'toaster', '$http', '$location', '$sessionStorage', '$localStorage', 'myUrl', function ($scope, toaster, $http, $location, $sessionStorage, $localStorage, myUrl) {
        if ($sessionStorage.UserId != null) {
            $http.get(myUrl + "api/SchoolByAdministratorId?AdministratorId=" + $sessionStorage.UserId).then(function (response) {
                var results = response.data;

                $scope.LogoUrl = results.LogoUrl;
                $scope.Name = results.Name;
                $sessionStorage.SchoolId = results.ID;
                $sessionStorage.LogoUrl = $scope.LogoUrl;
                $sessionStorage.Name = $scope.Name;

                $scope.loadPageFunctions();
            }, function (response) {
                //toaster.pop('error', response.data, "Check mobile number or password");
            });
        } else {
            $location.path('/login');
        }

        $scope.loadPageFunctions = function () {
            //Get Latest Fee Data
            $http.get(myUrl + "api/LatestFeesBySchoolId?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {
                $scope.LatestFees = response.data;
            }, function (response) {
            });

            //Get Latest events Data
            $http.get(myUrl + "api/LatestEventsBySchoolId?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {
                $scope.LatestEvents = response.data;
            }, function (response) {
            });

            //Get GraphsRow Data
            $http.get(myUrl + "api/GraphRow?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {

                $scope.TotalExams = response.data.TotalExams;
                $scope.TotalStudents = response.data.TotalStudents;

                $scope.AllTimeBestScore = response.data.AllTimeBestScore;
                $scope.BestSubject = response.data.BestSubject;
                $scope.WorstSubject = response.data.WorstSubject;

                $scope.Paid = response.data.TotalPaid;
                $scope.Unpaid = response.data.TotalUnpaid;

                $scope.chartResults= {
                    palette: "bright",
                    dataSource: response.data.GradeSummary,
                    title: "Grade Summary",
                    legend: {
                        orientation: "horizontal",
                        itemTextPosition: "bottom",
                        horizontalAlignment: "center",
                        verticalAlignment: "bottom",
                        columnCount: 5
                    },
                    "export": {
                        enabled: true
                    },
                    series: [{
                        argumentField: "Grade",
                        valueField: "Total",
                        label: {
                            visible: true,
                            font: {
                                size: 16
                            },
                            connector: {
                                visible: true,
                                width: 0.5
                            },
                            position: "columns",
                            customizeText: function (arg) {
                                return arg.valueText + " (" + arg.percentText + ")";
                            }
                        }
                    }]
                };

                $scope.chartSubjects = {
                    dataSource: response.data.TopSubjects,
                    commonSeriesSettings: {
                        argumentField: "Term",
                        type: "bar",
                        hoverMode: "allArgumentPoints",
                        selectionMode: "allArgumentPoints",
                        label: {
                            visible: true,
                            format: {
                                type: "fixedPoint",
                                precision: 0
                            }
                        }
                    },
                    series: [
                        {valueField: "TopSubject", name: "Top Subject"},
                        {valueField: "SecondHighestSubject", name: "Second"},
                        {valueField: "ThirdHighestSubject", name: "Third"}
                    ],
                    title: "Top Subjects",
                    legend: {
                        verticalAlignment: "bottom",
                        horizontalAlignment: "center"
                    },
                    "export": {
                        enabled: true
                    },
                    onPointClick: function (e) {
                        e.target.select();
                    }
                };

                $scope.chartFinance = {
                    palette: "violet",
                    dataSource: response.data.FeesData,
                    commonSeriesSettings: {
                        argumentField: "Month"
                    },
                    bindingOptions: {
                        "commonSeriesSettings.type": "line"
                    },
                    margin: {
                        bottom: 20
                    },
                    argumentAxis: {
                        valueMarginsEnabled: false,
                        discreteAxisDivisionMode: "crossLabels",
                        grid: {
                            visible: true
                        }
                    },
                    series: [
                        {valueField: "Paid", name: "Paid"},
                        {valueField: "Unpaid", name: "Unpaid"},
                    ],
                    legend: {
                        verticalAlignment: "bottom",
                        horizontalAlignment: "center",
                        itemTextPosition: "bottom"
                    },
                    title: {
                        text: 'School Finance',
                        subtitle: {
                            text: "(Monthly)"
                        }
                    },
                    "export": {
                        enabled: true
                    },
                    tooltip: {
                        enabled: true,
                        customizeTooltip: function (arg) {
                            return {
                                text: arg.valueText
                            };
                        }
                    }
                };
            }, function (response) {
            });

            //Get StatsRow Data
            $http.get(myUrl + "api/StatsRow?SchoolId=" + $sessionStorage.SchoolId).then(function (response) {

                $scope.Students = response.data.Students;
                $scope.Parents = response.data.Parents;
                $scope.Events = response.data.Events;
                $scope.Vacancies = response.data.Vacancies;
            }, function (response) {
            });
        }



        if($localStorage.first_time_count == null) {
            // first time loaded!
            startIntro();
            $localStorage.first_time_count = 1;
        }















    }]);