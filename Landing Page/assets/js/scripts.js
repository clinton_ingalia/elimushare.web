/*
Author       : Themes_Mountain.
Template Name: Tollfree - App template
Version      : 1.0
*/

(function($) {
	'use strict';
	
	jQuery(document).on('ready', function(){
	
		/*PRELOADER JS*/
		$(window).on('load', function() { 
			$('.status').fadeOut();
			$('.preloader').delay(350).fadeOut('slow'); 
		}); 
		/*END PRELOADER JS*/

		/*START MENU JS*/
			$('a.page-scroll').on('click', function(e){
				var anchor = $(this);
				$('html, body').stop().animate({
					scrollTop: $(anchor.attr('href')).offset().top - 50
				}, 1500);
				e.preventDefault();
			});		

			$(window).on('scroll', function() {
			  if ($(this).scrollTop() > 100) {
				$('.menu-top').addClass('menu-shrink');
			  } else {
				$('.menu-top').removeClass('menu-shrink');
			  }
			});			
			
			$(document).on('click','.navbar-collapse.in',function(e) {
			if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
				$(this).collapse('hide');
			}
			});				
		/*END MENU JS*/ 
			
			$("#modal_trigger").leanModal({top : 200, overlay : 0.6, closeButton: ".modal_close" });

			$(function(){
				// Calling Login Form
				$("#login_form").on('click', function(){
					$(".social_login").hide();
					$(".user_login").show();
					return false;
				});

				// Calling Register Form
				$("#register_form").on('click', function(){
					$(".social_login").hide();
					$(".user_register").show();
					$(".header_title").text('Register');
					return false;
				});

				// Going back to Social Forms
				$(".back_btn").on('click', function(){
					$(".user_login").hide();
					$(".user_register").hide();
					$(".social_login").show();
					$(".header_title").text('Login');
					return false;
				});

			})
		
			
	}); 			
	
				
})(jQuery);


  

